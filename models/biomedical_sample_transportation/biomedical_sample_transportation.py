# Given a set of sample collection centers (SCC) and labs, a set of vehicles and routes. 
# For each pair of SCC, it is associated the distance, travel time and a set of requests. 
# We wish to minimize the total transportation cost while satisfying capacity and time 
# limit constraints.

from pyomo.environ import *

model = AbstractModel("Biomedical Sample Transportation")

#Sets and parameters

#set of sample collection centers
model.scc = Set()

#set of all labs in the network
model.labs = Set()

#set of all nodes in the network
model.nodes = model.scc | model.labs

#set of arcs
def arcs(model):
    return ((i,j) for i in model.nodes for j in model.nodes if (i != j and i != 7 and j != 1))
model.arcs = Set(dimen = 2, initialize = arcs)

#travel times between each pair of nodes
model.t = Param(model.arcs, within = NonNegativeReals)

#distances between each pair of nodes
model.d = Param(model.arcs, within = NonNegativeReals)

#set of uncapacitated vehicles
model.vehicles = Set()

#set of routes
model.routes = Set()

#length of working day for a vehicle
model.work_length = Param(model.vehicles, within = NonNegativeReals)

#number of transportation request for each scc
model.transp_req =Set(model.scc, within = NonNegativeIntegers)

#time window for q-th request for scc i
def scc_req_arcs(model):
    return ((i,q) for i in model.scc for q in model.transp_req[i])
model.scc_req_arcs = Set(dimen = 2, initialize = scc_req_arcs)
model.scc_req_lb = Param(model.scc_req_arcs, within = NonNegativeReals)
model.scc_req_ub = Param(model.scc_req_arcs, within = NonNegativeReals)

#loading time at ech scc
model.load_scc = Param(model.scc, within = NonNegativeReals)

#unloading time at the lab
model.unload_lab = Param(within = NonNegativeReals)

#maximal transportation time for any sample
model.T_max = Param(within = NonNegativeReals)

#Variables

#binary variable determining whether arc (i,j) is used by vehicle k in its route r
model.x = Var(model.arcs, model.vehicles, model.routes, within = Binary)

#binary variable determining whether the q-th request of scc j is done by vehicle k in its route r
model.y = Var(model.scc_req_arcs, model.vehicles, model.routes, within = Binary)

#continuous variables that indicates the visit time of SCC i by vehicle k in route r
model.u = Var(model.nodes, model.vehicles, model.routes, within = NonNegativeReals)

#Objective

#minimizing transportation costs
def min_transp_costs(model):
    return sum(sum(sum(model.d[i,j]*model.x[i,j,k,r] for (i,j) in model.arcs) for k in model.vehicles) for r in model.routes)
model.min_transp_costs = Objective(rule = min_transp_costs)

#Constraints

#constraints indicating that an scc j is visited at most one time by route r of vehicle k
def con1(model, j, k, r):
    return sum(model.x[i,j,k,r] for i in model.nodes if (i,j) in model.arcs) <= 1
model.con1 = Constraint(model.scc, model.vehicles, model.routes, rule = con1)

#constraints forcing the flow of each vehicle k for each of its routes r to be balanced at each scc of the network
def con2(model, j, k, r):
    return sum(model.x[i,j,k,r] for i in model.nodes if (i,j) in model.arcs) - sum(model.x[j,l,k,r] for l in model.nodes if (j,l) in model.arcs) == 0
model.con2 = Constraint(model.scc, model.vehicles, model.routes)

#constraints stating that a truck k can start at most a route r
def con3(model, k, r):
    return sum(model.x[1,j,k,r] for j in model.scc if (1,j) in model.arcs) <= 1
model.con3 = Constraint(model.vehicles, model.routes, rule = con3)

#If a vehicle starts a route it must come back to depot
def con4(model, k, r):
    return sum(model.x[1,j,k,r] for j in model.scc if (1,j) in model.arcs) - sum(model.x[j,7,k,r] for j in model.scc if (j,7) in model.arcs) == 0
model.con4 = Constraint(model.vehicles, model.routes, rule = con4)

#Route r is started if and only if a route r-1 has been already created
def con5(model, k, r):
    if r == 1:
        return Constraint.Skip
    else:
        return sum(model.x[1,j,k,r] for j in model.scc if (1,j) in model.arcs) - sum(model.x[1,j,k,r-1] for j in model.scc if (1,j) in model.arcs) <= 0
model.con5 = Constraint(model.vehicles, model.routes, rule = con5)

#Each pick-up q for each scc j is done by one and only one vehicle route (k,r) combination
def con6(model, j, q):
    return sum(sum(model.y[j,q,k,r] for k in model.vehicles) for r in model.routes) == 1
model.con6 = Constraint(model.scc_req_arcs, rule = con6)

#If a pick-up is done by the route r of the truck k it is because there is an arc of this combination that entered node j
def con7(model, j, k, r):
    return sum(model.y[j,q,k,r] for q in model.transp_req[j]) - sum(model.x[i,j,k,r] for i in model.nodes if (i != 7 and (i,j) in model.arcs)) == 0 
model.con7 = Constraint(model.scc, model.vehicles, model.routes, rule = con7)

#Constraints estimating the arrival time at every node and forcing sub-tours elimination
def con8(model, i, j , k, r):
    if i not in set([1,7]) and j != 1 and i != j:
        return model.u[i,k,r] + model.load_scc[i] + model.t[i,j] - model.u[j,k,r] <= model.work_length[k]*(1 - model.x[i,j,k,r])
    else:
        return Constraint.Skip
model.con8 = Constraint(model.nodes, model.nodes, model.vehicles, model.routes, rule = con8)

#Constraints forcing that if the pick-up q of client j is done with vehicle k in its route r, it must be inside the time window of the pick-up request
def con9(model, j, k, r, q):
    return model.scc_req_lb[j,q] - model.work_length[k]*(1 - model.y[j,q,k,r]) <= model.u[j,k,r]
model.con9 = Constraint(model.scc_req_arcs, model.vehicles, model.routes)

#Constraints forcing that if the pick-up q of client j is done with vehicle k in its route r, it must be inside the time window of the pick-up request
def con10(model, j, k, r, q):
    return model.u[j,k,r] <= model.scc_req_ub[j,q] + model.work_length[k]*(1 - model.y[j,q,k,r])
model.con10 = Constraint(model.scc_req_arcs, model.vehicles, model.routes)

#The starting time of route r is later than the arriving time of route r-1 at depot
def con11(model, k, r):
    if r == 1:
        return Constraint.Skip
    else:
        return model.u[1,k,r] >= model.u[7,k,r-1]
model.con11 = Constraint(model.vehicles, model.routes, rule = con11)

#Limit on the maximum transportation length time
def con12(model, j, k, r):
    return model.u[7,k,r] - model.u[j,k,r] <= model.T_max + (1 - sum(model.x[i,j,k,r] for i in model.nodes if (i != 7 and (i,j) in model.arcs)))*model.work_length[k]
model.con12 = Constraint(model.scc, model.vehicles, model.routes, rule = con12)

#Constraints satisfying the total work shift length for a vehicle k
def con13(model, k, r):
    return model.u[7,k,r] - model.u[1,k,r] <= model.work_length[k]
model.con13 = Constraint(model.vehicles, model.routes, rule = con13)


#Python script for printing the solution in the terminal
solver = SolverFactory("cbc")
instance = model.create_instance("biomedical_sample_transportation.dat")
results = solver.solve(instance)

for (i,j) in instance.arcs:
    for k in instance.vehicles:
        for r in instance.routes:
            if value(instance.x[i,j,k,r]) > 0:
                print("Vehicle %s uses route %s to pass through arc (%s,%s)" %(k,r,i,j))
for (j,q) in instance.scc_req_arcs:
    for k in instance.vehicles:
        for r in instance.routes:
            if value(instance.y[j,q,k,r]) > 0:
                print("Request %s at the sample collection center %s is done by vehicle %s through route %s" %(q,j,k,r))
for i in instance.nodes:
    for k in instance.vehicles:
        for r in instance.routes:
            if value(instance.u[i,k,r]) > 0:
                print("The visit time of sample collection center %s is done by vehicle %s in route %s" %(i,k,r))
print("The minimum transportation cost is: %f" %value(instance.min_transp_costs))
        
            
#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in instance.arcs:
        for k in instance.vehicles:
            for r in instance.routes:
                if value(instance.x[i,j,k,r]) > 0:
                    output.write("Vehicle %s uses route %s to pass through arc (%s,%s)\n\n" %(k,r,i,j))
    for (j,q) in instance.scc_req_arcs:
        for k in instance.vehicles:
            for r in instance.routes:
                if value(instance.y[j,q,k,r]) > 0:
                    output.write("Request %s at the sample collection center %s is done by vehicle %s through route %s\n\n" %(q,j,k,r))
    for i in instance.nodes:
        for k in instance.vehicles:
            for r in instance.routes:
                if value(instance.u[i,k,r]) > 0:
                    output.write("The visit time of sample collection center %s is done by vehicle %s in route %s\n\n" %(i,k,r))
    output.write("The minimum transportation cost is: %f" %value(instance.min_transp_costs))
    output.close()
