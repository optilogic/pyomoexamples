# A number of cities are given and the capacity of every pair in terms of circuits. 
# Demand of circuits between cities are also given. The problem is to determine 
# whether it is possible to satisfy the demands entirely.

from pyomo.environ import *

model = AbstractModel("Routing telephone calls/MCNF")

#Sets and parameters

#set of arcs linking cities
model.arcs_between_cities = Set()

#maximum number of circuits between two cities
model.capacity = Param(model.arcs_between_cities, within = NonNegativeIntegers)

#set of pairs of cities between which there are demand for calls 
model.city_pairs = Set()

#demands between city pairs
model.demands = Param(model.city_pairs, within = NonNegativeIntegers)

#set of paths
model.paths = Set()

#set of arcs in each of the paths
model.arcs_in_each_path = Set(model.paths, within = model.arcs_between_cities)

#set of arcs linking the cities having calls with each other
model.begin_end = Set(model.paths)

#Variables

#integer variable indicating the amount of flow in each path
model.flow_path = Var(model.paths, within = NonNegativeIntegers)

#Objective

#maximizing the total flow throughout in the network
def max_flow(model):
    return sum(model.flow_path[f] for f in model.paths)
model.max_flow = Objective(rule = max_flow, sense = maximize)

#Constraints

#consider every arc i: the sum of flows on the paths passing through i must not exceed the capacity of i
def upper_bounded(model, i):
    return sum(model.flow_path[p] for p in model.paths if i in model.arcs_in_each_path[p]) <= model.capacity[i]
model.upper_bounded = Constraint(model.arcs_between_cities, rule = upper_bounded)

#the sum of flows exchanged between cities over the various paths must not exceed the demand for circuits
def bounded_by_demand(model, c):
    x = 0
    for p in model.paths:
        for w in model.begin_end[p]:
            if w == c:
                x += model.flow_path[p]
    return x <= model.demands[c]
model.bounded_by_demand = Constraint(model.city_pairs, rule = bounded_by_demand)


solver = SolverFactory("cbc")
instance = model.create_instance("routing_telephone_calls.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.paths:
    if value(instance.flow_path[i]) > 0:
        print(f'The amount of flow in path {i} is {value(instance.flow_path[i])}')
print(f'The maximum total flow throughout the network is {value(instance.max_flow)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.paths:
        if value(instance.flow_path[i]) > 0:
            output.write(f'The amount of flow in path {i} is {value(instance.flow_path[i])}\n\n')
    output.write(f'The maximum total flow throughout the network is {value(instance.max_flow)}')
    output.close()
