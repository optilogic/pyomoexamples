# Consider a company that produces bicycles. Sales forecast in thousand of units 
# for the coming year are given. The company has a capacity of 30,000 bicycles 
# per month. It is possible to augment the production by up to 50% through overtime 
# working, but this increases the production cost for a bicycle from the usual $32 to $40. 
# Currently there are 2,000 bicycles in stock. The storage costs have been calculated 
# as $5 per unit held in stock at the end of a month. The problem is to determine which 
# quantities need to be produced and stored in the course of the next twelve months in order 
# to satisfy the forecast demand and minimize the total cost.

from pyomo.environ import *

model=AbstractModel("Bicycle Production Planning")

#Sets and Parameters

#set of months
model.months = Set()

#demand per month
model.demand_per_month = Param(model.months, within = NonNegativeIntegers)

#cost per bicycle produced during normal hours
model.cost_bicyc_normal = Param(within = NonNegativeIntegers)

#cost per bicycle produced during overtime hours
model.cost_bicyc_overtime = Param(within = NonNegativeIntegers)

#cost per bicycle held in stock
model.cost_bicyc_stock = Param(within = NonNegativeIntegers)

#capacity of bicycles during normal hours, each month
model.cap = Param(within = NonNegativeIntegers)

#the initial stock of bicycles
model.initial_stock = Param(within = NonNegativeIntegers)

#Variables

#Variable indicating the number of bicycles produced during normal hours in month t
model.bicyc_normal = Var(model.months, within = NonNegativeIntegers)

#Variable indicating the number of bicycles produced during overtime hours in month t
model.bicyc_overtime = Var(model.months, within = NonNegativeIntegers)

#Variable indicating the number of bicycles held in stock each month
model.stock = Var(model.months, within = NonNegativeIntegers)

#Objective

#Objective function minimizing the cost of the production
def min_cost(model):
    return sum(model.cost_bicyc_normal*model.bicyc_normal[t] + model.cost_bicyc_overtime*model.bicyc_overtime[t] + model.cost_bicyc_stock* model.stock[t] for t in model.months)
model.min_cost = Objective(rule = min_cost)

#Constraints

#Inventory balance constraint for first month
def inventory_balance_1(model):
    return model.initial_stock + model.bicyc_normal[1] + model.bicyc_overtime[1] == model.stock[1] + model.demand_per_month[1]
model.inventory_balance_1 = Constraint(rule = inventory_balance_1)

#Inventory balance constraint for months 2-12
def inventory_balance(model, t):
    if t==1:
        return Constraint.Skip
    else:
        return model.stock[t-1] + model.bicyc_normal[t] + model.bicyc_overtime[t] == model.stock[t] + model.demand_per_month[t]
model.inventory_balance = Constraint(model.months, rule = inventory_balance)

#limit on the production during normal hours per month
def cap_normal(model, t):
    return model.bicyc_normal[t] <= model.cap
model.cap_normal = Constraint(model.months, rule = cap_normal)

#limit on the production during overtime per month
def cap_overtime(model, t):
    return model.bicyc_overtime[t] <= 0.5 * model.cap
model.cap_overtime = Constraint(model.months, rule = cap_overtime)

solver = SolverFactory("cbc")
instance = model.create_instance("bicycle_production_planning.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.months:
        if value(instance.bicyc_normal[i]) > 0:
            print(f'{value(instance.bicyc_normal[i])} bicycles need to be produced during normal hours on month {i}')
for i in instance.months:
    if value(instance.bicyc_overtime[i]) > 0:
        print(f'{value(instance.bicyc_overtime[i])} bicycles need to be produced during overtime hours on month {i}')
for i in instance.months:
    if value(instance.stock[i]) > 0:
        print(f'{value(instance.stock[i])} bicycles are held on stock on month {i}')
print(f'The minimum total cost of production is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.months:
        if value(instance.bicyc_normal[i]) > 0:
            output.write(f'{value(instance.bicyc_normal[i])} bicycles need to be produced during normal hours on month {i}\n\n')
    for i in instance.months:
        if value(instance.bicyc_overtime[i]) > 0:
            output.write(f'{value(instance.bicyc_overtime[i])} bicycles need to be produced during overtime hours on month {i}\n\n')
    for i in instance.months:
        if value(instance.stock[i]) > 0:
            output.write(f'{value(instance.stock[i])} bicycles are held on stock on month {i}\n\n')
    output.write(f'The minimum total cost of production is {value(instance.min_cost)}')
    output.close()
