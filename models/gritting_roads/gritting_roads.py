# In the case of ice, all the streets of a village need to be gritted. A set of streets 
# and their lengths are given. The truck has a sufficiently large capacity to grit all 
# the streets during a single tour. Due to the one-way streets, it may be forced to pass 
# several times through the same street that has already been gritted. It is wished to 
# determine a tour for the gritting truck of minimum total length that passes through 
# all streets. For bidirectional streets, both directions need to be gritted separately.

from pyomo.environ import *

model = AbstractModel("Gritting roads")

#Sets and parameters

#set of intersections
model.intersections = Set()

#set of arcs between intersections
model.arcs = Set(within = model.intersections*model.intersections)

#length of each arc(street)
model.lengths = Param(model.arcs, within = NonNegativeIntegers)

#Variables

#integer variable x indicating the number of times is passed through arc (i,j)   
model.x = Var(model.arcs, within = NonNegativeIntegers)

#Objective

#minimizing the total length while passing through each street at least once in a closed circuit starting at intersection 1
def min_length(model):
    return sum(model.lengths[i,j]*model.x[i,j] for (i,j) in model.arcs)
model.min_length = Objective(rule = min_length)

#Constraints

#number of arcs going in each intersection must be equal to the number of arcs out of that intersection
def no_arcs_in_equal_no_arcs_out(model, k):
    return sum(model.x[j,i] for (j,i) in model.arcs if i == k) == sum(model.x[i,j] for (i,j) in model.arcs if i == k)
model.no_arcs_in_equal_no_arcs_out = Constraint(model.intersections, rule = no_arcs_in_equal_no_arcs_out)

#every street must be passed at least once
def each_street_passed(model, i, j):
    return model.x[i,j] >= 1
model.each_street_passed = Constraint(model.arcs, rule = each_street_passed)


solver = SolverFactory("cbc")
instance = model.create_instance("gritting_roads.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for (i,j) in instance.arcs:
    if value(instance.x[i,j]) == 1:
        print("Street (%s,%s) is passed once" %(i,j))
    elif value(instance.x[i,j]) > 1:
        print("Street (%s,%s) is passed %d times" %(i,j,value(instance.x[i,j])))
print("The minimum length of crossing each street at least once is: %f" %value(instance.min_length))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in instance.arcs:
        if value(instance.x[i,j]) == 1:
            output.write("Street (%s,%s) is passed once\n\n" %(i,j))
        elif value(instance.x[i,j]) > 1:
            output.write("Street (%s,%s) is passed %d times\n\n" %(i,j,value(instance.x[i,j])))
    output.write("The minimum length of crossing each street at least once is: %f" %value(instance.min_length))
    output.close()
