# A company wishes to expand and has requested proposals for expansion projects for a planning period 
# of five years. Every project has an annual cost and is designed to produce a benefit after five years. 
# The problem is to determine which projects should the management choose now to maximize the total 
# benefit after five years.

from pyomo.environ import *

model = AbstractModel("Choice of expansion projects")

#Sets and parameters

#set of projects
model.projects = Set()

#set of time periods
model.periods = Set()

#the benefit from each project
model.benefit = Param(model.projects, within = NonNegativeReals)

#cost of each project in each year
model.cost = Param(model.projects, model.periods, within = NonNegativeReals)

#capital available for funding projects in each year
model.capital = Param(model.periods, within = NonNegativeReals)

#Variables

#binary variable indicating whether a project is chosen
model.choose = Var(model.projects, within = Binary)

#Objective

#maximizing the total profit
def max_profit(model):
    return sum(model.benefit[p]*model.choose[p] for p in model.projects)
model.max_profit = Objective(rule = max_profit, sense = maximize)

#Constraints

#the sum of the costs of the chosen projects does not exceed the available capital in year t
def cost_upper_bound(model, t):
    return sum(model.cost[p,t]*model.choose[p] for p in model.projects) <= model.capital[t]
model.cost_upper_bound = Constraint(model.periods, rule = cost_upper_bound)


solver = SolverFactory("cbc")
instance = model.create_instance("choice_of_expansion_projects.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.projects:
        if value(instance.choose[i]) > 0:
            print(f'Project {i} is chosen to expand')
print(f'The maximum profit is {value(instance.max_profit)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.projects:
        if value(instance.choose[i]) > 0:
            output.write(f'Project {i} is chosen to expand\n\n')
    output.write(f'The maximum profit is {value(instance.max_profit)}')
    output.close()
