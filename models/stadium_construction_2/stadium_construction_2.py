# In order to construct a stadium, a set of tasks and their completion time are given. 
# Some tasks can only start after the completion of certain other tasks. The council 
# is prepared to pay a bonus of $30K for every week the work finishes early. 
# The problem is to determine when will the project be completed if the builder wishes 
# to maximize his profit.

from pyomo.environ import *

model = AbstractModel("Construction of a stadium")

#Sets and parameters

#set of tasks
model.tasks = Set()

#duration of each task
model.duration = Param(model.tasks, within = NonNegativeIntegers)

#set of arcs (i,j): task j has task i as a predecessors
model.arcs = Set(within = model.tasks*model.tasks)

#the maximum number of weeks that can be reduced for each task
model.max_red = Param(model.tasks, within = NonNegativeIntegers)

#cost per week in each task
model.cost = Param(model.tasks, within = NonNegativeIntegers)

#bonus per week saved
model.bonus = Param(within = NonNegativeIntegers)

#Variables

#variable indicating the earliest start time of each task i
model.start = Var(model.tasks, within = NonNegativeIntegers)

#variable indicating the number of weeks wishing to save for each task
model.save = Var(model.tasks, within = NonNegativeIntegers)

#Objective

#maximizing the total saved time
def max_saving(model):
    return model.bonus * model.save[19] - sum(model.cost[i]*model.save[i] for i in model.tasks if i != 19)
model.max_saving = Objective(rule = max_saving, sense = maximize) 

#Constraints

#A task j can only start after task i is finished
def earliest_possible_start(model, i, j):
    return model.start[i] + model.duration[i] - model.save[i] <= model.start[j]
model.earliest_possible_start = Constraint(model.arcs, rule = earliest_possible_start)

#upper bound on the number of weeks to save for each task
def max_red_up_bound(model, i):
    if i == 19:
        return Constraint.Skip
    else:
        return model.save[i] <= model.max_red[i]
model.max_red_up_bound = Constraint(model.tasks, rule = max_red_up_bound)

#bound for save variable on the fictious task
def last_week(model):
    return model.start[19] == 64 - model.save[19]
model.last_week = Constraint(rule = last_week)


solver = SolverFactory("cbc")
instance = model.create_instance("stadium_construction_2.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.tasks:
    if value(instance.start[i]) > 0:
        print(f'The earliest start time of task {i} is {value(instance.start[i])}')
for i in instance.tasks:
    if value(instance.save[i]) > 0:
        print(f'The number of weeks wishing to save for task {i} is {value(instance.save[i])}')
print(f'The maximum total time saving is {value(instance.max_saving)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.tasks:
        if value(instance.start[i]) > 0:
            output.write(f'The earliest start time of task {i} is {value(instance.start[i])}\n\n')
    for i in instance.tasks:
        if value(instance.save[i]) > 0:
            output.write(f'The number of weeks wishing to save for task {i} is {value(instance.save[i])}\n\n')
    output.write(f'The maximum total time saving is {value(instance.max_saving)}')
    output.close()
