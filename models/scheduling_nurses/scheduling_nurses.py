# Determine the minimum number of nurses required to cover all the requirements, 
# knowing that a nurse works eight hours per day and that she is entitled to 
# a break of two hours after she has worked for four hours

from pyomo.environ import *

model = AbstractModel("Scheduling nurses")

#Sets and parameters

#number of time periods
model.number_time_periods = Param(within = NonNegativeIntegers)

#set of time periods; 12 time periods
model.time_periods = RangeSet(0,11)

#number of nurses needed during time period t
model.nurses_need = Param(model.time_periods, within = NonNegativeIntegers)

#max number of available nurses
model.max_number_nurses = Param(within = NonNegativeIntegers)

#Variables

#variable indicating the number of nurses needed to start at time period t
model.number_nurse = Var(model.time_periods, within = NonNegativeIntegers)

#variable indicating the number of nurses starting working at time period t and working two hours overtime at the end of the shift
model.number_nurse_overtime = Var(model.time_periods, within = NonNegativeIntegers)

#Objective

#minimizing the number of nurses needed
def min_nurses(model):
    return sum(model.number_nurse_overtime[t] for t in model.time_periods)
model.min_nurses = Objective(rule = min_nurses)

#Constraints

#the minimum number of nurses needed during time period t should be satisfied; the range of i can be modified according to the number of hours per shift and number of time periods
def req_satisfied(model, t):
    return model.number_nurse_overtime[(t-5+model.number_time_periods) % 12] + sum(model.number_nurse[(t+i+model.number_time_periods) % 12] for i in [0,-1,-3, -4] ) >= model.nurses_need[t]
model.req_satisfied = Constraint(model.time_periods, rule = req_satisfied)

#number of nurses starting at time period t and working overtime cannot exceed the overall number of nurses starting at t
def upper_bound_overtime(model, t):
    return model.number_nurse_overtime[t] <= model.number_nurse[t]
model.upper_bound_overtime = Constraint(model.time_periods, rule = upper_bound_overtime)

#number of overall nurses cannot exceed the number of available nurses
def upper_bound_nurses(model):
    return sum(model.number_nurse[t] for t in model.time_periods) <= model.max_number_nurses
model.upper_bound_nurses = Constraint(rule = upper_bound_nurses)

solver = SolverFactory("cbc")
instance = model.create_instance("scheduling_nurses.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.time_periods:
    if value(instance.number_nurse[i]) > 0:
        print(f'{value(instance.number_nurse[i])} nurses are needed to start at time period {i}')
for i in instance.time_periods:
    if value(instance.number_nurse_overtime[i]) > 0:
        print(f'{value(instance.number_nurse_overtime[i])} nurses are needed to start at time period {i} and work two hours overtime at the end of the shift')
print(f'The minimum number of nurses is {value(instance.min_nurses)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.time_periods:
        if value(instance.number_nurse[i]) > 0:
            output.write(f'{value(instance.number_nurse[i])} nurses are needed to start at time period {i}\n\n')
    for i in instance.time_periods:
        if value(instance.number_nurse_overtime[i]) > 0:
            output.write(f'{value(instance.number_nurse_overtime[i])} nurses are needed to start at time period {i} and work two hours overtime at the end of the shift\n\n')
    output.write(f'The minimum number of nurses is {value(instance.min_nurses)}')
    output.close()
