# It is wished to backup important files of different sizes onto floppy disks of a certain size. 
# The problem is to determine how should the files be distributed in order to minimize 
# the number of floppy disks used.

from pyomo.environ import *

model = AbstractModel("Backing up files")

#Sets and parameters

#set of files
model.files = Set()

#set of empty disks
model.disks = Set()

#capacity of empty disks
model.cap = Param(within = NonNegativeReals)

#size of each file
model.file_size = Param(model.files, within = NonNegativeReals)

#Variables

#binary variable save_fd indicating whether a file is saved in disk d
model.save_fd = Var(model.files, model.disks, within = Binary)

#binary variable use_d indicating whether a disk d is used
model.use_d = Var(model.disks, within = Binary)

#Objective

#minimize the number of disks that are used
def min_number_disks(model):
    return sum(model.use_d[d] for d in model.disks)
model.min_number_disks = Objective(rule = min_number_disks)

#Constraints

#every file uses exactly one disk
def each_file_one_disk(model, f):
    return sum(model.save_fd[f,d] for d in model.disks) == 1
model.each_file_one_disk = Constraint(model.files, rule = each_file_one_disk)

#the size of files in a disk should not exceed the capacity of the disk
def capacity_satisfied(model, d):
    return sum(model.file_size[f]*model.save_fd[f,d] for f in model.files) <= model.cap*model.use_d[d]
model.capacity_satisfied = Constraint(model.disks, rule = capacity_satisfied)


solver = SolverFactory("cbc")
instance = model.create_instance("backing_up_files.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.files:
    for j in instance.disks:
        if value(instance.save_fd[i,j]) > 0:
            print(f'File {i} is saved on disk {j}')
for i in instance.disks:
    if value(instance.use_d[i]) > 0:
        print(f'Disk {i} is used')
print(f'The minimum number of disks that are used is {value(instance.min_number_disks)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.files:
        for j in instance.disks:
            if value(instance.save_fd[i,j]) > 0:
                output.write(f'File {i} is saved on disk {j}\n\n')
    for i in instance.disks:
        if value(instance.use_d[i]) > 0:
            output.write(f'Disk {i} is used\n\n')
    output.write(f'The minimum number of disks that are used is {value(instance.min_number_disks)}')
    output.close()
