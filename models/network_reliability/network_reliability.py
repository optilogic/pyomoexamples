# Consider a military telecommunications network consisting of eleven sites 
# connected by bidirectional lines for data transmission. Two of the eleven 
# sites are able to communicate even if any three other sites of the network 
# are destroyed. The problem is to determine whether there are at least 4 
# disjoint paths between two nodes in a given graph.

from pyomo.environ import *

model = AbstractModel("Network Reliability")

#Sets and paramters

#set of sites
model.sites = Set()

#set of sources
model.sources = Set()

#set of sinks
model.sinks = Set()

#union of the set of sources and sinks
model.nodes = model.sources | model.sinks | model.sites

#set of arcs between sites
model.arcs = Set(within = model.nodes*model.nodes)

#capacity of each arc
def cap(model):
    for (i,j) in model.arcs:
        return 1
model.capacity = Param(model.arcs, initialize = cap)

#Variables

#variable representing the amount of flow passing from site i to site j
model.flow = Var(model.arcs, within = Binary)

#Objective

#maximizing the sum of all flows on the arcs leaving the source
def max_flow(model):
    return sum(model.flow[10,s] for s in model.sites if (10,s) in model.arcs)
model.max_flow = Objective(rule = max_flow, sense = maximize)

#Constraints

#avoid the flow injected at source returning to this node
def no_source_inflow(model):
    return sum(model.flow[s, 10] for s in model.nodes if (s, 10) in model.arcs) == 0
model.no_source_inflow = Constraint(rule = no_source_inflow)

#the flow through every intermediate node is limited to 1
def limit_outflow(model, s):
    return sum(model.flow[s,t] for t in model.nodes if (s,t) in model.arcs) <= 1
model.limit_outflow = Constraint(model.sites, rule = limit_outflow)

#conservation constraint at every site
def inflow_equal_outflow(model, s):
    return sum(model.flow[s,t] for t in model.nodes if (s,t) in model.arcs) == sum(model.flow[t,s] for t in model.nodes if (t,s) in model.arcs)
model.inflow_equal_outflow = Constraint(model.sites, rule = inflow_equal_outflow)


solver = SolverFactory("cbc")
instance = model.create_instance("network_reliability.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for (i,j) in instance.arcs:
    if value(instance.flow[i,j]) > 0:
        print(f'The amount of flow moving from site {i} to site {j} is {value(instance.flow[i,j])}')
print(f'The maximum amount of flow over all arcs leaving the source is {value(instance.max_flow)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in instance.arcs:
        if value(instance.flow[i,j]) > 0:
            output.write(f'The amount of flow moving from site {i} to site {j} is {value(instance.flow[i,j])}\n\n')
    output.write(f'The maximum amount of flow over all arcs leaving the source is {value(instance.max_flow)}')
    output.close()
