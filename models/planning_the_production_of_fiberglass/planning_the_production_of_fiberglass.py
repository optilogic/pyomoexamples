# A company produces fiberglass by the cubic meter and wishes to plan its 
# production for the next six weeks. The production capacity is limited, 
# and this limit takes a different value in every time period. The weekly 
# demand is given for the entire planning period. The production and storage 
# costs also take different values depending on the time period. The problem 
# is to determine which is the production plan that minimizes the total cost 
# of production and storage.

from pyomo.environ import *

model = AbstractModel("Planning the production of fiberglass")

#Sets and parameters

#set of production nodes
model.prod_nodes = Set()

#set of demand nodes
model.demand_nodes = Set()

#set of all nodes
model.nodes = model.prod_nodes | model.demand_nodes

#set of arcs
model.arcs = Set(within = model.nodes * model.nodes)

#production and storage costs
model.prod_storage_costs = Param(model.arcs, within = NonNegativeReals)

#production capacity and demand
model.prod_cap_demand = Param(model.nodes, within = NonNegativeIntegers)

#Variables

#variables indicating the amount of flow on each arc
model.flow = Var(model.arcs, within = NonNegativeReals)

#Objective

#minimizing the total flow cost
def min_cost(model):
    return sum(model.prod_storage_costs[m,n]*model.flow[m,n] for (m,n) in model.arcs)
model.min_cost = Objective(rule = min_cost)

#Constraints

#the quantity stored at the end of the period 1 equals the amount produced in period 1 minus the demand DEM 2
def storage_quantity(model):
    return model.flow[2,4] == model.flow[1,2] - model.prod_cap_demand[2]
model.storage_quantity = Constraint(rule = storage_quantity)

#No stock for the last period
def no_stock(model):
    return model.flow[10,12] + model.flow[11,12] - model.prod_cap_demand[12] == 0
model.no_stock = Constraint(rule = no_stock)

#stock balance
def stock_balance(model, n):
    if n == 2 or n == 12:
        return Constraint.Skip
    else:
        return model.flow[n,n+2] == model.flow[n-2,n] + model.flow[n-1,n] - model.prod_cap_demand[n]
model.stock_balance = Constraint(model.demand_nodes, rule = stock_balance)

#production capacity
def capacity_satisfied(model, n):
    return model.flow[n,n+1] <= model.prod_cap_demand[n]
model.capacity_satisfied = Constraint(model.prod_nodes, rule = capacity_satisfied)


solver = SolverFactory("cbc")
instance = model.create_instance("planning_the_production_of_fiberglass.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for (i,j) in instance.arcs:
    if value(instance.flow[i,j]) > 0:
        print(f'The amount of flow passing through arc ({i},{j}) is {value(instance.flow[i,j])}')
print(f'The minimum total flow cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in instance.arcs:
        if value(instance.flow[i,j]) > 0:
            output.write(f'The amount of flow passing through arc ({i},{j}) is {value(instance.flow[i,j])}\n\n')
    output.write(f'The minimum total flow cost is {value(instance.min_cost)}')
    output.close()
