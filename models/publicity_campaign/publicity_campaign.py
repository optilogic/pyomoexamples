# A company wishes to advertise a product that is about to release. 
# The company has a budget on marketing and there are 6 media types 
# on which the product will be advertised. For each medium there is 
# a cost associated and a forecasted number of people on which the 
# media has impact on. Within the budget, the company wishes to 
# determine which media to use and in what proportion by maximizing 
# the index of perception quality.

from pyomo.environ import *

model = AbstractModel("Publicity Campaign")

#Sets and parameters

#set of medias
model.media_types = Set()

#limit on the use of each media
model.max_use = Param(model.media_types, within = NonNegativeIntegers)

#the number of people each media tends to reach
model.number_people_attract = Param(model.media_types, within = NonNegativeIntegers)

#budget of the company on publicity
model.budget = Param(within = NonNegativeReals)

#minimum number of people to be reached after advertising
model.min_requirement = Param( within = NonNegativeIntegers)

#the cost per unit of using each media
model.unit_cost = Param(model.media_types, within = NonNegativeReals)

#the perception quality for each media
model.quality = Param(model.media_types, within = NonNegativeIntegers)

#Variables

#variable indicating number of use for each media
model.number_use = Var(model.media_types, within = NonNegativeIntegers)

#Objective

#maximizing the sum of the quality indices of every media used.
def max_quality_index(model):
    return sum(model.quality[m]*model.number_use[m] for m in model.media_types)
model.max_quality_index = Objective(rule = max_quality_index, sense = maximize)

#Constraints

#the sum of money spent on publicity should not exceed the budget
def budget_limit(model):
    return sum(model.unit_cost[m]*model.number_use[m] for m in model.media_types) <= model.budget
model.budget_limit = Constraint(rule = budget_limit)

#limit on the number of use for each media
def media_use_limit(model, m):
    return model.number_use[m] <= model.max_use[m]
model.media_use_limit = Constraint(model.media_types, rule = media_use_limit)

#number of people expected to be reached should be at least as large as the required number
def target_reached(model):
    return sum(model.number_people_attract[m]*model.number_use[m] for m in model.media_types) >= model.min_requirement
model.target_reached = Constraint(rule = target_reached)


solver = SolverFactory("cbc")
instance = model.create_instance("publicity_campaign.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.media_types:
        if value(instance.number_use[i]) > 0:
            print(f'Number of uses of medium {i} is {value(instance.number_use[i])}')
print(f'The maximum sum of the quality indices of every media used is {value(instance.max_quality_index)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.media_types:
        if value(instance.number_use[i]) > 0:
            output.write(f'Number of uses of medium {i} is {value(instance.number_use[i])}\n\n')
    output.write(f'The maximum sum of the quality indices of every media used is {value(instance.max_quality_index)}')
    output.close()
