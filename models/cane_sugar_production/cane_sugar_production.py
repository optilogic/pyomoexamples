# The harvest of cane sugar in a country is highly mechanized. The sugar cane is 
# immediately transported to a sugar house in wagons that run on a network of small 
# rail tracks. Once harvested, the sugar content decreases rapidly through 
# fermentation and the wagon load will entirely lose its value after a certain time. 
# At this moment, eleven wagons all loaded with the same quantity have arrived at the 
# sugar house. They have been examined to find out the hourly loss and the remaining 
# life span of every wagon. The processing of a lot takes two hours. It must be finished 
# at the latest at the end of the life span of the wagon load. The problem is to determine 
# a production schedule for the currently available lots that minimizes the total loss of sugar.

from pyomo.environ import *

model = AbstractModel("Cane sugar production")

#Sets and parameters

#number of production lines
model.numb_prod_lines = Param(within = NonNegativeIntegers)

#set of wagons
model.wagons = Set()

#set of slots
model.slots = Set()

#duration of the production process for every lot
model.duration = Param(within = NonNegativeReals)

#the hourly loss for every wagon
model.loss = Param(model.wagons, within = NonNegativeIntegers)

#Life span for every wagon
model.lifespan = Param(model.wagons, within = NonNegativeIntegers)

#Variables

#binary variable deciding whether wagon w is processed at slot s
model.process = Var(model.wagons, model.slots, within = Binary)

#Objective

#minimizing total loss of sugar
def min_loss(model):
    return sum(sum(s*model.duration*model.loss[w]*model.process[w,s] for s in model.slots) for w in model.wagons)
model.min_loss = Objective(rule = min_loss)

#Constraints

#Every lot needs to be assigned to a slot
def lot2slot(model, w):
    return sum(model.process[w,s] for s in model.slots) == 1
model.lot2slots = Constraint(model.wagons, rule = lot2slot)

#any slot may take up to NL lots
def lot_limit_per_slot(model, s):
    return sum(model.process[w,s] for w in model.wagons) <= model.numb_prod_lines
model.lot_limit_per_slot = Constraint(model.slots, rule = lot_limit_per_slot)

#The maximum slot number for a wagon load w
def max_slot_numb_per_wagon(model, w):
    return sum(s*model.process[w,s] for s in model.slots) <= model.lifespan[w] / model.duration
model.max_slot_numb_per_wagon = Constraint(model.wagons, rule = max_slot_numb_per_wagon)



solver = SolverFactory("cbc")
instance = model.create_instance("cane_sugar_production.dat")
results = solver.solve(instance)


#Python Script for printing the solution in the terminal
for i in instance.wagons:
    for j in instance.slots:
        if value(instance.process[i,j]) > 0:
            print(f'Wagon {i} is processed at slot {j}')
print(f'The minimum total loss of sugar is {value(instance.min_loss)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.wagons:
        for j in instance.slots:
            if value(instance.process[i,j]) > 0:
                output.write(f'Wagon {i} is processed at slot {j}\n\n')
    output.write(f'The minimum total loss of sugar is {value(instance.min_loss)}')
    output.close()
