# In this problem from molecular biology it is wished to predict the
# structure of the folded protein sequence. For simplicity, only protein
# sequences with two type of amino acids, hydrophilic and hydrophobic,
# are considered. When folded, this protein tries to match as many
# hydrphobics as possible. 

from pyomo.environ import *

model = AbstractModel("Protein folding")

#Sets and parameters

#set of hydrophobic (water hating) amino acids in the sequence
model.hydrophobic = Set()

#set of hydrophilic (water loving) amino acids in the sequence
model.hydrophilic = Set()

#set of all amino acids in the sequence
model.amino_acids = model.hydrophobic | model.hydrophilic

#set of all pairs
def pairs(model):
    return ((i,j) for i in model.hydrophobic for j in model.hydrophobic if (i < j and j != i + 1))
model.pairs = Set(dimen = 2, initialize = pairs)

#Variables

#binary variable y_i indicating whether protein is folded between i-th and (i+1)-th acids in the sequence
model.y = Var(model.amino_acids, within = Binary)

#binary variable x_ij indicating whether hydrophobic i is matched to hydrophobic j, i < j and j is not i + 1
model.x = Var(model.pairs, within = Binary)

#Objective

#maximizing the number of matches among hydrophobics in the sequence
def max_hpho(model):
    return sum(model.x[i,j] for (i,j) in model.pairs)
model.max_hpho = Objective(rule = max_hpho, sense = maximize)

#Constraints

#two non-consecutive hydrophobics can be matched by folding the protein if they have an even number of acids between them
def even_numb_acids(model, i, j):
    if (j + i - 1) % 2 == 0:
        return model.x[i,j] == model.y[(j + i - 1)/2]
    else:
        return Constraint.Skip    
model.even_numb_acids = Constraint(model.pairs, rule = even_numb_acids)

#constraints on non-matching hydrophobics
def non_matching_hpho(model, i, j):
    if (j + i - 1) % 2 != 0:
        return model.x[i,j] == 0
    else:
        return Constraint.Skip
model.non_matching_hpho = Constraint(model.pairs, rule = non_matching_hpho)

#two non-consecutive hydrophobics cannot be matched unless the protein is folded at the midpoint between these hydrophobics
def even_numb(model, i, j, k):
    if i + 1 <= k and k < j and k != (j + i - 1) / 2:
        return model.y[k] + model.x[i,j] <= 1
    else:
        return Constraint.Skip
model.even_numb = Constraint(model.pairs, model.amino_acids, rule = even_numb)



#Python script for printing the solution in the terminal
solver = SolverFactory("cbc")
instance = model.create_instance("protein_folding.dat")
results = solver.solve(instance)

print("Non-consecutive hydrophibic amino acid matchings after optimal protein folding: ")
for (i,j) in instance.pairs:
    if value(instance.x[i,j]) > 0:
        print("Hydrophobic amino acid %s matches hydrophobic amino acid %s" %(i,j))
print("The maximum number of non-consecutive hydrophobic amino acid matchings %d" %value(instance.max_hpho))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("Non-consecutive hydrophobic amino acid matchings after optimal protein folding:\n\n")
    for (i,j) in instance.pairs:
        if value(instance.x[i,j]) > 0:
            output.write("Hydrophobic amino acid %s matches hydrophobic amino acid %s\n\n" %(i,j))
    output.write("The maximum number of non-consecutive hydrophobic amino acid matchings %d" %value(instance.max_hpho))
    output.close()
