# Assume there are a set of cities that need to be water supplied by a 
# set of reservoirs through pumping stations. Each reservoir has a fixed 
# availability and each pipe in the network has a certain capacity. 
# Each city has a certain demand that need to be satisfied in a decade. 
# We wish to maximize the amount of flow to the sink, while demand constraints are satisfied.

from pyomo.environ import *

model = AbstractModel("Water Supply Management/ Maximum flow problem")

#Sets and parameters

#set of all nodes
model.nodes = Set()

#set of arcs between nodes
model.arcs = Set(within=model.nodes*model.nodes)

#set of sources as a subset of nodes
model.sources = Set(within=model.nodes)

#set of sinks as a subset of nodes
model.sinks = Set(within=model.nodes)

#capacities on each arc
model.capacity = Param(model.arcs, within = NonNegativeIntegers)

#Variables

#variable flow indicating the amount of flow from i to j 
model.flow = Var(model.arcs, within=NonNegativeIntegers)

#Objective

#maximizing the flow 
def max_flow(model):
    objective = sum(model.flow[i,12] for (i,j) in model.arcs if j in model.sinks)
    return objective
model.max_flow = Objective(rule = max_flow, sense = maximize)

#Constraints

#conservation constraints
def conservation(model, k):
    if k in model.sources:
        return Constraint.Skip
    elif k in model.sinks:
        return Constraint.Skip
    else:
        return sum(model.flow[i,j] for (i,j) in model.arcs if j == k ) == sum(model.flow[i,j] for (i,j) in model.arcs if i == k)
model.conservation = Constraint(model.nodes, rule = conservation)

#capacity constraints
def cap(model, i,j):
    return model.flow[i,j] <= model.capacity[i,j]
model.cap = Constraint(model.arcs, rule = cap)


solver = SolverFactory("cbc")
instance = model.create_instance("water_supply_management.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for (i,j) in instance.arcs:
    if value(instance.flow[i,j]) > 0:
        print("The amount of water from node %s to node %s is: %f" %(i,j,value(instance.flow[i,j])))
print("The maximum amount of flow is: %f" %value(instance.max_flow))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in instance.arcs:
        if value(instance.flow[i,j]) > 0:
            output.write("The amount of water from node %s to node %s is: %f\n\n" %(i,j,value(instance.flow[i,j])))
    output.write("The maximum amount of flow is: %f" %value(instance.max_flow))
    output.close()
