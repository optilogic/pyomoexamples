# Due to flooding in a country only seven runways are usable including the capital. 
# It is decided that each airplane has to leave from the capital and visit every other 
# airport before returning to the capital. The distances between every two airports are 
# given. The problem is to determine the order that the airports have to be visited so 
# as to minimize the total distance covered.

from pyomo.environ import *

model = AbstractModel("Planning a flight tour")

#Sets and parameters

#set of airports excluding the capital city airport
model.airports = Set()

#set of undirected arcs between aiports
def arcs(model):
    return ((i,j) for i in model.airports for j in model.airports if i != j)
model.arcs = Set(dimen = 2, initialize = arcs)

#distances between airports
model.distances = Param(model.arcs, within = NonNegativeReals)

#Variables

#binary variables indicating whether a plane should fly from an airport to another
model.fly = Var(model.arcs, within = Binary)

#auxiliary variable needed for subtour elimination constraints
model.u = Var(model.airports, within = NonNegativeReals)

#Objective

#minimizing total length of the cycle passing through all cities
def min_length(model):
    return sum(model.distances[i,j]*model.fly[i,j] for (i,j) in model.arcs)
model.min_length = Objective(rule = min_length)

#Constraints

#every airport is visited once and only once
def each_airport_visited_once(model, j):
    return sum(model.fly[i,j] for i in model.airports if (i,j) in model.arcs) == 1
model.each_airport_visited_once = Constraint(model.airports, rule = each_airport_visited_once)

#for every airport there is only one succeding airport
def one_airport_succeding(model, i):
    return sum(model.fly[i,j] for j in model.airports if (i,j) in model.arcs) == 1
model.one_airport_succeding = Constraint(model.airports, rule = one_airport_succeding)

#subtour elimination constraints
def subtour_elimination(model, i, j):
    if i > 1 and i < 7 and j > 1 and j < 7:
        return 7*model.fly[i,j] + model.u[i] - model.u[j] <= 6
    else:
        return Constraint.Skip
model.subtour_elimination = Constraint(model.arcs, rule = subtour_elimination)


solver = SolverFactory("cbc")
instance = model.create_instance("planning_a_flight_tour.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.airports:
    for j in instance.airports:
        if (i,j) in instance.arcs and value(instance.fly[i,j]) > 0:
            print(f'A plane should fly from airport {i} to airport {j}')
print(f'The minimum total length of the cycle passing through all cities is {value(instance.min_length)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.airports:
        for j in instance.airports:
            if (i,j) in instance.arcs and value(instance.fly[i,j]) > 0:
                output.write(f'A plane should fly from airport {i} to airport {j}\n\n')
    output.write(f'The minimum total length of the cycle passing through all cities is {value(instance.min_length)}')
    output.close()
