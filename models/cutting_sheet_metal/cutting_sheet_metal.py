# A sheet metal workshop cuts pieces of sheet metal from large rectangular sheets 
# of 48 decimeters × 96 decimeters. It has received an order for 8 rectangular pieces 
# of 36 dm × 50 dm, 13 sheets of 24 dm × 36 dm, 5 sheets of 20 dm × 60 dm, and 15 sheets 
# of 18 dm × 30 dm. The problem is to determine how this order can be satisfied by using 
# the least number of large sheets.

from pyomo.environ import *

model = AbstractModel("Cutting Sheet Metal")

#Sets and parameters

#set of patterns needed
model.patterns = Set()

#set of different sizes
model.diff_sizes = Set()

#demand for each size
model.demand = Param(model.diff_sizes, within = NonNegativeIntegers)

#cost for using pattern p
model.cost = Param(within = NonNegativeIntegers)

#number of size s in pattern p
model.size_pattern = Param(model.diff_sizes, model.patterns, within = NonNegativeIntegers)

#Variables

#integer variable indicating the number of times a sheet metal is cut using pattern p
model.number_cut_p = Var(model.patterns, within = NonNegativeIntegers)

#Objective

#minimizing the number of large sheets needed
def min_sheets(model):
    return sum(model.cost*model.number_cut_p[p] for p in model.patterns)
model.min_sheets = Objective(rule = min_sheets)

#Constraints

#demand satisfaction constraints
def demand_satisfied(model, s):
    return sum(model.size_pattern[s,p]*model.number_cut_p[p] for p in model.patterns) >= model.demand[s]
model.demand_satisfied = Constraint(model.diff_sizes, rule = demand_satisfied)


solver = SolverFactory("cbc")
instance = model.create_instance("cutting_sheet_metal.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.patterns:
        if value(instance.number_cut_p[i]) > 0:
            print(f'Pattern {i} is used {value(instance.number_cut_p[i])} times to cut a large sheet metal')
for i in instance.diff_sizes:
    for j in instance.patterns:
        if value(instance.size_pattern[i,j]) > 0:
            print(f'Size {i} appears {value(instance.size_pattern[i,j])} times in pattern {j}')
print(f'The minimum number of large sheets needed is {value(instance.min_sheets)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.patterns:
        if value(instance.number_cut_p[i]) > 0:
            output.write(f'Pattern {i} is used {value(instance.number_cut_p[i])} times to cut a large sheet metal\n\n')
    for i in instance.diff_sizes:
        for j in instance.patterns:
            if value(instance.size_pattern[i,j]) > 0:
                output.write(f'Size {i} appears {value(instance.size_pattern[i,j])} times in pattern {j}\n\n')
    output.write(f'The minimum number of large sheets needed is {value(instance.min_sheets)}')
    output.close()
