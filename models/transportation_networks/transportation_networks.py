# The transportation problem deals with the distribution of a commodity
# from a set of sources to a set of destinations. The objective is to
# minimize total transportation costs while satisfying constraints on
# the shipments from all sources which cannot exceed the manufacturing
# capacity of the source, and satisfying demand requirements at each of
# the destinations.

from pyomo.environ import *
import matplotlib.pyplot as plt

model = ConcreteModel("Transportation Networks")

#Data
Demand = {
   'Lon':   125,        # London
   'Ber':   175,        # Berlin
   'Maa':   225,        # Maastricht
   'Ams':   250,        # Amsterdam
   'Utr':   225,        # Utrecht
   'Hag':   200         # The Hague
}

Supply = {
   'Arn':   600,        # Arnhem
   'Gou':   650         # Gouda
}

Cost = {
    ('Lon','Arn'): 1000,
    ('Lon','Gou'): 2.5,
    ('Ber','Arn'): 2.5,
    ('Ber','Gou'): 1000,
    ('Maa','Arn'): 1.6,
    ('Maa','Gou'): 2.0,
    ('Ams','Arn'): 1.4,
    ('Ams','Gou'): 1.0,
    ('Utr','Arn'): 0.8,
    ('Utr','Gou'): 1.0,
    ('Hag','Arn'): 1.4,
    ('Hag','Gou'): 0.8
}

#Parameters

customers = list(Demand.keys())
sources = list(Supply.keys())

#Variables

#variable representing the amount of flow in the network from a source to a customer
model.flow = Var(customers, sources, within = NonNegativeReals)

#Objective

#minimizing the total shipping cost to all customers from all sources
model.cost = Objective(expr = sum([Cost[c,s]*model.flow[c,s] for c in customers for s in sources]))

#Constraints

#Shipments from all sources can not exceed the manufacturing capacity of the source
model.src = ConstraintList()
for s in sources:
    model.src.add(sum([model.flow[c,s] for c in customers]) <= Supply[s])

#Shipments to each customer must satisfy their demand    
model.dmd = ConstraintList()
for c in customers:
    model.dmd.add(sum([model.flow[c,s] for s in sources]) == Demand[c])

    
model.dual = Suffix(direction=Suffix.IMPORT)
    
solver = SolverFactory('cbc')
results = solver.solve(model)



#Printing the solution
if 'ok' == str(results.Solver.status):
    print("Total Shipping Costs = %f" %value(model.cost))
    print("\nShipping Table:")
    for s in sources:
        for c in customers:
            if value(model.flow[c,s]) > 0:
                print('Ship from %s to %s: %f' %(s, c, value(model.flow[c,s])))
else:
    print("No Valid Solution Found")



#Sensitivity Analysis
#Analysis by source
if 'ok' == str(results.Solver.status):
    print("\nSources:")
    print("Source      Capacity   Shipped    Margin")
    for m in model.src.keys():
        s = sources[m-1]
        print("{0:10s}{1:10.1f}{2:10.1f}{3:10.4f}".format(s,Supply[s],model.src[m](),model.dual[model.src[m]]))
else:
    print("No Valid Solution Found")

#Analysis by customer
if 'ok' == str(results.Solver.status):    
    print("\nCustomers:")
    print("Customer      Demand   Shipped    Margin")
    for n in model.dmd.keys():
        c = customers[n-1]
        print("{0:10s}{1:10.1f}{2:10.1f}{3:10.4f}".format(c,Demand[c],model.dmd[n](),model.dual[model.dmd[n]]))
else:
    print("No Valid Solution Found")


shipping = []
for i in model.dmd.keys():
    shipping.append(value(model.dual[model.dmd[i]]))
plt.bar(customers, shipping, align = 'center')
plt.xlabel("Customers")
plt.ylabel("Margin")
plt.title("Sensitivity Analysis")
plt.savefig("Sensitivity Analysis.pdf")
#plt.show()


#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("Total Shipping Costs = %f\n\n" %value(model.cost))
    output.write("\nShipping Table:\n\n")
    for s in sources:
        for c in customers:
            if value(model.flow[c,s]) > 0:
                output.write('Ship from %s to %s: %f\n\n' %(s, c, value(model.flow[c,s])))
    output.write("\nSources:\n\n")
    output.write("Source      Capacity   Shipped    Margin\n\n")
    for m in model.src.keys():
        s = sources[m-1]
        output.write("{0:10s}{1:10.1f}{2:10.1f}{3:10.4f}\n\n".format(s,Supply[s],model.src[m](),model.dual[model.src[m]]))
    output.write("\nCustomers:\n\n")
    output.write("Customer      Demand   Shipped    Margin\n\n")
    for n in model.dmd.keys():
        c = customers[n-1]
        output.write("{0:10s}{1:10.1f}{2:10.1f}{3:10.4f}\n\n".format(c,Demand[c],model.dmd[n](),model.dual[model.dmd[n]]))
    output.close()
