# In order to construct a stadium, a set of tasks and their completion time are given. 
# Some tasks can only start after the completion of certain other tasks. 
# The problem is to determine which is the earliest possible time of completing the construction.

from pyomo.environ import *

model = AbstractModel("Construction of a stadium")

#Sets and parameters

#set of tasks
model.tasks = Set()

#duration of each task
model.duration = Param(model.tasks, within = NonNegativeIntegers)

#set of arcs (i,j): task j has task i as a predecessors
model.arcs = Set(within = model.tasks*model.tasks)

#Variables

#variable indicating the earliest start time of each task i
model.start = Var(model.tasks, within = NonNegativeIntegers)

#Objective

#minimize the total time
def min_time(model):
    return model.start[19]
model.min_time = Objective(rule = min_time) 

#Constraints

#A task j can only start after task i is finished
def earliest_possible_start(model, i, j):
    return model.start[i] + model.duration[i] <= model.start[j]
model.earliest_possible_start = Constraint(model.arcs, rule = earliest_possible_start)

solver = SolverFactory("cbc")
instance = model.create_instance("stadium_construction_1.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.tasks:
    if value(instance.start[i]) > 0:
        print(f'The earliest start time of task {i} is {value(instance.start[i])}')
print(f'The minimum total time is {value(instance.min_time)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.tasks:
        if value(instance.start[i]) > 0:
            output.write(f'The earliest start time of task {i} is {value(instance.start[i])}\n\n')
    output.write(f'The minimum total time is {value(instance.min_time)}')
    output.close()
