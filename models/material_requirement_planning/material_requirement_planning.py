# A company produces two types of large toy lorries for children: blue removal vans 
# and red tank lorries. Each of these lorries is assembled from thirteen items. 
# The breakdown of the components as well as the prices of the components are given. 
# The subsets may be assembled by the company using the components, or subcontracted. 
# Costs for assembling and for subcontracting these subsets, together with the capacities 
# of the company are given as well. For the next month, the company has the same demand 
# forecast of 3000 pieces for the two types of lorries. The problem is to determine which 
# quantities of the different items should the company buy or subcontract to satisfy the 
# demand while minimizing the production cost.

from pyomo.environ import *

model = AbstractModel("Material requirement planning")

#Sets and parameters

#set of final products
model.final = Set()

#set of products obtained from assembly
model.assembly = Set()

#set of products used in the assembly of others
model.initial_products = Set()

#set of arcs
model.arcs = Set(within = model.assembly*model.initial_products)

#set of all products
model.products = model.final | model.assembly | model.initial_products

#number of products of a type needed in the assembly of another product
model.numb_prod_needed = Param(model.arcs, within = NonNegativeReals)

#cost of buying a product
model.cost_buy_product = Param(model.initial_products, within = NonNegativeReals)

#cost of assembling a product
model.cost_assembly = Param(model.assembly, within = NonNegativeReals)

#demand for each final product
model.demand = Param(model.final, within = NonNegativeIntegers)

#capacity of each assembly product
model.assembly_cap = Param(model.assembly, within = NonNegativeIntegers)

#Variables

#variable representing the number of preproducts to buy
model.buy = Var(model.initial_products, within = NonNegativeIntegers)

#variable representing the quantities of the assembled product to be produced
model.produce = Var(model.assembly, within = NonNegativeIntegers)

#Objective

#minimizing the production cost
def min_prod_cost(model):
    return sum(model.buy[p]*model.cost_buy_product[p] for p in model.initial_products) + sum(model.produce[p]*model.cost_assembly[p] for p in model.assembly)
model.min_prod_cost = Objective(rule = min_prod_cost)

#Constraints

#upper bound on the number of assembly products to produce
def ub_assembly_prod(model, p):
    return model.produce[p] <= model.assembly_cap[p]
model.ub_assembly_prod = Constraint(model.assembly, rule = ub_assembly_prod)

#demand of the final products satisfied
def demand_satisfied(model, p):
    return model.produce[p] >= model.demand[p]
model.demand_satisfied = Constraint(model.final, rule = demand_satisfied)

#the total quantity of item p that is bought or produced is sufficient to produce all products q that contain p
def sufficient_raw_material(model, p):
    if p in model.assembly:
        return model.buy[p] + model.produce[p] >= sum(model.numb_prod_needed[q,p]*model.produce[q] for q in model.assembly if (q,p) in model.arcs)
    else:
        return model.buy[p] >= sum(model.numb_prod_needed[q,p]*model.produce[q] for q in model.assembly if (q,p) in model.arcs)
model.sufficient_raw_material = Constraint(model.initial_products, rule = sufficient_raw_material)    


solver = SolverFactory("cbc")
instance = model.create_instance("material_requirement_planning.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.initial_products:
    if value(instance.buy[i]) > 0:
        print(f'{value(instance.buy[i])} {i} are needed to buy')
for i in instance.assembly:
    if value(instance.produce[i]) > 0:
        print(f'{value(instance.produce[i])} {i} will be produced')
print(f'The minimum total cost of production is {value(instance.min_prod_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.initial_products:
        if value(instance.buy[i]) > 0:
            output.write(f'{value(instance.buy[i])} {i} are needed to buy\n\n')
    for i in instance.assembly:
        if value(instance.produce[i]) > 0:
            output.write(f'{value(instance.produce[i])} {i} will be produced\n\n')
    output.write(f'The minimum total cost of production is {value(instance.min_prod_cost)}')
    output.close()
