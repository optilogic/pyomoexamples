# A transporter has to deliver heating oil from the refinery to a certain number of clients. 
# The demand for each client as well as the distances between any pair of clients or a client 
# and the refinery are given. The transport company uses tankers with a capacity of 39000 liters 
# for the deliveries. The problem is to determine the tours for delivering to all clients 
# that minimize the total number of kilometers driven.

from pyomo.environ import *

model = AbstractModel("Heating oil delivery")

#Sets and parameters

#set of clients
model.clients = Set()

#demand per client
model.demand = Param(model.clients, within = NonNegativeReals)

#set containing the refinery
model.refinery = Set()

#union of refinery set and clients set
model.ref_union_clients = model.refinery | model.clients

#set of arcs between every two places
def arcs(model):
    return ((i,j) for i in model.ref_union_clients for j in model.ref_union_clients)
model.arcs = Set(dimen = 2, initialize = arcs)

#distances between every two places
model.distances = Param(model.arcs, within = NonNegativeReals)

#capacity of each tanker
model.capacity = Param(within = NonNegativeReals)

#Variables

#variable indicating the total amount of oil delivered on the route to client i, up to and including client i
model.amount = Var(model.clients, within = NonNegativeReals)

#variable indicating if client i immediately precedes client j in a tour
model.preceding = Var(model.ref_union_clients, model.ref_union_clients, within = Binary)

#Objective

#minimizing the total amount travelled
def min_travel(model):
    return sum(sum(model.distances[i,j]*model.preceding[i,j] for j in model.ref_union_clients if j != i) for i in model.ref_union_clients)
model.min_travel = Objective(rule = min_travel)

#Constraints

#there must be exactly one client preceding another client
def prec_1(model, j):
    return sum(model.preceding[i,j] for i in model.ref_union_clients if i != j) == 1
model.prec_1 = Constraint(model.clients, rule =prec_1)

#there must be exactly one client succeding another client
def prec_2(model, i):
    return sum(model.preceding[i,j] for j in model.ref_union_clients if j != i) == 1
model.prec_2 = Constraint(model.clients, rule =prec_2)

#the total amount of oil delivered on the route to client i, up to and including client i should not exceed the capacity of each tanker
def cap(model, i):
    return model.amount[i] <= model.capacity
model.cap = Constraint(model.clients, rule = cap)

#the total amount of oil delivered on the route to client i, up to and including client i should satisfy the demands
def dem(model, i):
    return model.demand[i] <= model.amount[i]
model.dem = Constraint(model.clients, rule = dem)

#upper bound on the total amount of oil delivered on the route to client i, up to and including client i
def ub_amount(model, i):
    return model.amount[i] <= model.capacity + (model.demand[i] - model.capacity)*model.preceding[1,i]
model.ub_amount = Constraint(model.clients, rule = ub_amount)

#amount[i] must equal the sum of quantities delivered between the refinery and i inclusively. This means that if client j comes after client i in a tour, we can write that quant j must be equal to the quantity delivered on the tour from the refinery to i, plus the quantity ordered by j
def relation_ij(model, i, j):
    if i == j:
        return Constraint.Skip
    else:
        return model.amount[j] >= model.amount[i] + model.demand[j] - model.capacity + model.capacity*model.preceding[i,j] + (model.capacity - model.demand[j] - model.demand[i])*model.preceding[j,i]  
model.relation_ij = Constraint(model.clients, model.clients, rule = relation_ij)


solver = SolverFactory("cbc")
instance = model.create_instance("heating_oil_delivery.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.clients:
    if value(instance.amount[i]) > 0:
        print(f'The total amount of oil delivered on the route to client {i}, up to and including client {i} is {value(instance.amount[i])}')
print(f'The minimum of the total amount travelled is {value(instance.min_travel)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.clients:
        if value(instance.amount[i]) > 0:
            output.write(f'The total amount of oil delivered on the route to client {i}, up to and including client {i} is {value(instance.amount[i])}\n\n')
    output.write(f'The minimum of the total amount travelled is {value(instance.min_travel)}')
    output.close()
