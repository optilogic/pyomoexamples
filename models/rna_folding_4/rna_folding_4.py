# Let S be a RNA string of length n constructed from the nucleotides
# A, U, C, G. We wish to build a Mixed Integer Linear Programming model
# which will predict the secondary structure of the given RNA
# sequence. There are several constraint to be satisfied.

# First, with the exception of the noncomplementary pairs (G,U)
# any other pair in the RNA folding must be a complementary pair,
# i.e., pairs must fall into one of the following ordered pairs
# (A,U), (U,A), (C,G), (G,C), (G,U).

# Second, each nucleotide in the sequence must pair to at most one other
# nucleotide in the sequence.

# Thirdly, there cannot be crossing among any two pairs.

# Lastly, any pair of nucleotides must satisfy a minimum distance given.

# The objective is to maximize the number of pairings while giving
# preference to complementary pairs (G,C) and (C,G) when there is a
# choice. This problem is formulated below as a binary program.

from pyomo.environ import *

model = ConcreteModel("RNA Folding 4")

#Enter your RNA sequence below:
RNA = "GCGCGCGCGCGUGUGUGUGUGUGGCGCGCGCGC"

def split(word): 
    return [char for char in word]  
nucleotides = split(RNA)
list1 = list(range(len(nucleotides)))

index_nucleotide = {} 
for key in list1: 
    for value in nucleotides: 
        index_nucleotide[key] = value 
        nucleotides.remove(value) 
        break  

    
list2 = []    
for i in index_nucleotide:
    for j in index_nucleotide:
        if i < j:
            list2.append((i,j))

#Enter below the minimum distance of two matched nucleotides:            
model.mindist = 2

#Variables

#binary variable indicating whether two different nucleotides are paired
model.pairing = Var(list2, within = Binary)


#Objective

#maximizing the number of pairings
def max_pairs(model):
    x = 0
    for (i,j) in list2:
        if (index_nucleotide[i] == 'C' and index_nucleotide[j] == 'G') or (index_nucleotide[i] == 'G' and index_nucleotide[j] == 'C'):
            x += 1.33*model.pairing[i,j]
        else:
            x += model.pairing[i,j]
    return x
model.max_pairs = Objective(rule = max_pairs, sense = maximize)



#Constraints

model.non_matching = ConstraintList()
for (i,j) in list2:
    if index_nucleotide[i] == 'A' and index_nucleotide[j] != 'U':
        model.non_matching.add(model.pairing[i,j] == 0)
    elif index_nucleotide[i] == 'U' and index_nucleotide[j] != 'A':
        model.non_matching.add(model.pairing[i,j] == 0)
    elif index_nucleotide[i] == 'C' and index_nucleotide[j] != 'G':
        model.non_matching.add(model.pairing[i,j] == 0)
    elif index_nucleotide[i] == 'G' and index_nucleotide[j] != 'C' and index_nucleotide[j] != 'U':
        model.non_matching.add(model.pairing[i,j] == 0)

model.matched_at_most_once = ConstraintList()
for i in index_nucleotide:
    model.matched_at_most_once.add(sum(model.pairing[i,j] for j in index_nucleotide if (i,j) in list2) + sum(model.pairing[j,i] for j in index_nucleotide if (j,i) in list2) <= 1)

model.no_crossing = ConstraintList()
for (i,j) in list2:
    for (k,l) in list2:
        if i < k and  k < j and j < l:
            model.no_crossing.add(model.pairing[i,j] + model.pairing[k,l] <= 1)

#any two nucleotides that have distance at most mindist cannot be matched
model.min_dist = ConstraintList()
for (i,j) in list2:
    if j - i <= model.mindist:
        model.min_dist.add(model.pairing[i,j] == 0)


            
solver = SolverFactory("cbc")
results = solver.solve(model)

#Python Script for printing the solution in the terminal
for (i,j) in list2:
    if model.pairing[i,j] == 1:
        print("Nucleotide %s at position %s is matched to nucleotide %s at position %s" %(index_nucleotide[i], i, index_nucleotide[j], j))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in list2:
        if model.pairing[i,j] == 1:
            output.write("Nucleotide %s at position %s is matched to nucleotide %s at position %s\n\n" %(index_nucleotide[i], i, index_nucleotide[j], j))
    output.close()

