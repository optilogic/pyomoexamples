# A food is manufactured by refining raw oils and blending them together
# on a $6$-months planning horizon. The final product has a fixed price
# per ton. There are two categories of raw oils, veggy and
# non-veggy. There is a capcity on the amount of each oil category to be
# blended each month. Also, raw oil can be stored each month for later
# use for a fixed cost per ton per month. For each raw oil it is
# associated a coefficient indicating hardness of the raw oil. The
# hardness of the final product must lie within two given bounds. In
# addition to constraints in Food Manufacture 1 model the following
# constraints are added:

# 1. The food may never be made up of more than three oils in any
# month.

# 2. If an oil is used in any month, there is a lower bound on the amount
# of the oil used.

# 3. The use of one of the oils is implied by the use of
# two other oils.

from pyomo.environ import *

model = AbstractModel("Food Manufacture 2")

#Sets and parameters

#set of months
model.months = Set()

#set of veggy raw oil 
model.veg_oil_types = Set()

#set of non veggy raw oil
model.non_veg_oil_types = Set()

#set of all types of raw oils
model.oil_types = model.veg_oil_types | model.non_veg_oil_types

#cost of buying one unit of a type of raw oil for each month
model.buy_cost = Param(model.oil_types, model.months, within = NonNegativeReals)

#price for selling each unit of final product
model.price = Param(within = NonNegativeReals)

#limit on amount to refine each month the veg oil types
model.limit_veg = Param(within = NonNegativeReals)

#limit on amount to refine each month the non veg oil types
model.limit_non_veg = Param(within = NonNegativeReals)

#cost per ton each month of storing raw oil of each type
model.storage_cost = Param(within = NonNegativeReals)

#hardness for each raw oil type
model.hardness = Param(model.oil_types, within = NonNegativeReals)

#Variables

#amount of veg oil type to buy each month
model.buy = Var(model.oil_types, model.months, within = NonNegativeReals)

#amount to store veg oil types each month
model.store = Var(model.oil_types, model.months, within = NonNegativeReals)

#amount stored at the beginning of month 1
model.init_store = Var(model.oil_types, within = NonNegativeReals)

#amount used of each raw oil per month
model.used = Var(model.oil_types, model.months, within = NonNegativeReals)

#amount to produce each month
model.produce = Var(model.months, within = NonNegativeReals)

#binary variable indicating whether an oil is used in production in a month
model.oil_used = Var(model.oil_types, model.months, within = Binary)

#Objective

#maximizing profit
def max_profit(model):
    return sum(model.price*model.produce[t] for t in model.months) - sum(sum(model.buy_cost[i,t]*model.buy[i,t] for t in model.months) for i in model.oil_types) - sum(sum(model.storage_cost*model.store[i,t] for i in model.oil_types) for t in model.months if t != 6) - sum(model.storage_cost*model.init_store[i] for i in model.oil_types)
model.max_profit = Objective(rule = max_profit, sense = maximize)

#Constraints

#The amount stored in the previous month + amount bought in the current month must match the amount stored in the current month + amount used in the current month
def balance(model, i, t):
    if t == 1:
        return model.buy[i,1] + model.init_store[i] == model.store[i,1] + model.used[i,1]
    elif t == 6: 
        return model.buy[i,6] + model.store[i,5] == model.used[i,6] + 500
    else:
        return model.buy[i,t] + model.store[i,t-1] == model.used[i,t] + model.store[i,t]
model.balance = Constraint(model.oil_types, model.months, rule = balance)

#Amount of raw oil bought per month must be equal to amount used
def amount_prod(model, t):
    return sum(model.used[i,t] for i in model.oil_types) == model.produce[t]
model.amount_prod = Constraint(model.months, rule = amount_prod)

#The hardness in the final product must be at least 3
def hardness_sat1(model, t):
    return sum(model.hardness[i]*model.used[i,t] for i in model.oil_types) >= 3*model.produce[t]
model.hardness_sat1 = Constraint(model.months, rule = hardness_sat1)

#The hardness in the final product must be at most 6
def hardness_sat2(model, t):
    return sum(model.hardness[i]*model.used[i,t] for i in model.oil_types) <= 6*model.produce[t]
model.hardness_sat2 = Constraint(model.months, rule = hardness_sat2)

#In each month it is not possible to refine more than 200 tons of vegetable oils
def limit1(model, t):
    return sum(model.used[i,t] for i in model.veg_oil_types) <= model.limit_veg
model.limit1 = Constraint(model.months, rule = limit1)

#In each month it is not possible to refine more than 250 tons of non-vegetable oils
def limit2(model, t):
    return sum(model.used[i,t] for i in model.non_veg_oil_types) <= model.limit_non_veg
model.limit2 = Constraint(model.months, rule = limit2)

#amount stored at the beginning of the time horizon for each product
def fixed_init_store(model, i):
    return model.init_store[i] == 500
model.fixed_init_store = Constraint(model.oil_types, rule = fixed_init_store)

#if an oil type is used the amount used will be at least 20 in that month
def lb_oil_used(model, i, t):
    return model.used[i,t] >= 20*model.oil_used[i,t]
model.lb_oil_used = Constraint(model.oil_types, model.months, rule = lb_oil_used)

#if a veggy oil type is used the amount used will be at most 200 in that month
def ub_oil_used(model, i, t):
    return model.used[i,t] <= 200*model.oil_used[i,t]
model.ub_oil_used = Constraint(model.veg_oil_types, model.months, rule = ub_oil_used)

#if a non-veggy oil type is used the amount used will be at most 250 in that month
def ub_nv_oil_used(model, i, t):
    return model.used[i,t] <= 250*model.oil_used[i,t]
model.ub_nv_oil_used = Constraint(model.non_veg_oil_types, model.months, rule = ub_nv_oil_used)

#The food may never be made up of more than 3 oil types
def ub_no_oil_types(model, t):
    return sum(model.oil_used[i,t] for i in model.oil_types) <= 3
model.ub_no_oil_types = Constraint(model.months, rule = ub_no_oil_types)

#If any of the veggy oil types is used than the third non-veggy oil type should be used too  
def implication_cons(model, i, t):
    if i in model.veg_oil_types:
        return model.oil_used[i,t] <= model.oil_used[5,t]
    else:
        return Constraint.Skip
model.implication_cons = Constraint(model.oil_types, model.months, rule = implication_cons)


#Python script for printing the solution in the terminal
solver = SolverFactory("cbc")
instance = model.create_instance("food_manufacture_2.dat")
results = solver.solve(instance)

for i in instance.oil_types:
    for t in instance.months:
        if value(instance.buy[i,t]) > 0:
            print("On month %s will be bought %f of oil type %s" %(t,value(instance.buy[i,t]),i))
for i in instance.oil_types:
    for t in instance.months:
        if value(instance.used[i,t]) > 0:
            print("On month %s will be used %f of oil type %s" %(t,value(instance.used[i,t]),i))
for i in instance.oil_types:
    for t in instance.months:
        if t != 6:
            if value(instance.store[i,t]) > 0:
                print("On month %s will be stored %f of oil type %s" %(t,value(instance.store[i,t]),i))
for t in instance.months:
    if value(instance.produce[t]) > 0:
        print("On month %s will be produced %f of final product" %(t,value(instance.produce[t])))
print("The maximum profit is: %f" %value(instance.max_profit))

        
#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.oil_types:
        for t in instance.months:
            if value(instance.buy[i,t]) > 0:
                output.write("On month %s will be bought %f of oil type %s \n\n" %(t,value(instance.buy[i,t]),i))
    for i in instance.oil_types:
        for t in instance.months:
            if value(instance.used[i,t]) > 0:
                output.write("On month %s will be used %f of oil type %s \n\n" %(t,value(instance.used[i,t]),i))
    for i in instance.oil_types:
        for t in instance.months:
            if t != 6:
                if value(instance.store[i,t]) > 0:
                    output.write("On month %s will be stored %f of oil type %s \n\n" %(t,value(instance.store[i,t]),i))
    for t in instance.months:
        if value(instance.produce[t]) > 0:
            output.write("On month %s will be produced %f of final product \n\n" %(t,value(instance.produce[t])))
    output.write("The maximum profit is: %f" %value(instance.max_profit))
    output.close()
