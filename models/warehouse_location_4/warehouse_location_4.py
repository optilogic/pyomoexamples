# A company wishes to determine the optimal locations to build
# warehouses so as the delivery demands are met at a minimum cost. Let's
# assume that a set of possible warehouse locations and a set of
# customers are given. Nevertheless, costs for serving each customer to
# each location are given. The company wishes to built at most 2
# warehouses. Facilities can be hospitals, fire stations, ambulances,
# restaurants, schools, etc.

from pyomo.environ import *
import matplotlib.pyplot as plt


model = ConcreteModel("Warehouse Location 4")

#Candidate warehouse locations
N = ['Harlingen', 'Memphis', 'Ashland']

#Customer  locations
M = ['NYC', 'LA', 'Chicago', 'Houston']

#cost of serving a customer at a location
d ={('Harlingen', 'NYC'): 1956,
    ('Harlingen', 'LA'): 1606,
    ('Harlingen', 'Chicago'): 1410,
    ('Harlingen', 'Houston'): 330,
    ('Memphis', 'NYC'): 1096,
    ('Memphis', 'LA'): 1792,
    ('Memphis', 'Chicago'): 531,
    ('Memphis', 'Houston'): 567,
    ('Ashland', 'NYC'): 485,
    ('Ashland', 'LA'): 2322,
    ('Ashland', 'Chicago'): 324,
    ('Ashland', 'Houston'): 1236}

#number of warehouses that can be build
P = 2


#variables
model.assign = Var(N, M, bounds = (0,1))
model.build = Var(N, within = Binary)


#Objective

#minimizing the total cost
def min_cost(model):
    return sum(d[n,m]*model.assign[n,m] for m in M for n in N)
model.min_cost = Objective(rule = min_cost)


#Constraints

#assign each customer to exactly one warehouse
def cust_warehouse(model, m):
    return sum(model.assign[n,m] for n in N) == 1
model.cust_warehouse = Constraint(M, rule = cust_warehouse)

#if a warehouse is not build, then there should no customers assigned to that warehouse
def implic_con(model, n, m):
    return model.assign[n,m] <= model.build[n]
model.implic_con = Constraint(N, M, rule = implic_con)

#no more than 2 warehouses should be built
def numb_warehouses(model):
    return sum(model.build[n] for n in N) <= P
model.numb_warehouses = Constraint(rule = numb_warehouses)



opt = SolverFactory('cbc')

model.integer_cuts = ConstraintList()
objective_values = list()
done = False
iter = 0
while not done:
    results = opt.solve(model)
    iter += 1
    if results.solver.termination_condition != TerminationCondition.optimal:
        done = True
    else:
        objective_values.append(value(model.min_cost))
        N_true = [i for i in N if value(model.build[i]) > 0.5]
        N_false = [i for i in N if value(model.build[i]) < 0.5]
        expr1 = sum(model.build[i] for i in N_true)
        expr2 = sum(model.build[i] for i in N_false)
        model.integer_cuts.add( expr1 - expr2 <= len(N_true) - 1)
        output = open('results%d.txt'%value(model.min_cost), 'w')
        print('\n\nSolution %s\n\n' %iter)
        for n in N:		
            if value(model.build[n]) > 0:
                output.write('\n\nWarehouse %s will serve the following customers:' % n)   
                print('Warehouse %s will serve the following customers:' % n) 
                for m in M:
                    if value(model.assign[n,m]) > 0:
                        output.write('\n%s' % m)
                        print('%s' %m)
        output.close()
 

x = range(len(objective_values))
plt.bar(x, objective_values, align='center')
plt.xlabel('Solution Number')
plt.ylabel('Optimal Obj.Value')
plt.title("Warehouse Location Problem")
plt.savefig('Warehouse.pdf')

        
            
