# Given a set of time periods, set of items to be produced, set of production 
# resources/machines, we wish to minimize the setup and inventory holding cost 
# while satisfying several constraints on demand and production capacity.

from pyomo.environ import *

model = AbstractModel("Inventory and Lot Sizing")

#Sets and parameters

#set of all items
model.tot_items = Set()

#set of machines
model.machines = Set()

#set of items to be produced in each machine
model.subset_machines = Set(model.machines, within = model.tot_items)

#set of time periods
model.tperiods = Set()

#set of end items
model.endp = Set(within = model.tot_items)

#set of items that are not end items
model.subassemblies = model.tot_items - model.endp

#set of immediate successors of item i
model.succ = Set(model.subassemblies, within = model.tot_items)

#setup cost for producing a lot of an item
model.sc = Param(model.tot_items, within = NonNegativeReals)

#inventory holding cost for one unit of an item remaining at the end of a period
model.hc = Param(model.tot_items, within = NonNegativeReals)

#setup time required for producing an item on a machine
model.st = Param(model.tot_items, model.machines, within = NonNegativeReals)

#production time required to produce a unit of an item on a machine
model.prod_time = Param(model.tot_items, model.machines, within = NonNegativeReals)

#external demand for an item in a time period
model.ext_dem = Param(model.tot_items, model.tperiods, within = NonNegativeReals)

#number of units of an item needed to produce one unit of an immediate succesor item
def subassemb_to_successor(model):
    return ((i,j) for i in model.subassemblies for j in model.succ[i])
model.subassemb_to_successor = Set(dimen = 2, initialize = subassemb_to_successor)
model.numb_subassemb_prod = Param(model.subassemb_to_successor, within = NonNegativeIntegers)

#available capacity of a machine in a time period
model.capacity = Param(model.machines, model.tperiods, within = NonNegativeReals)

#maximum number of units of an item that can be produced in a time period
model.max_units = Param(model.tot_items, model.tperiods, within = NonNegativeReals)

#Variables

#variable indicating the number of units of item i produced in period t
model.x = Var(model.tot_items, model.tperiods, within = NonNegativeReals)

#variable indicating inventory of item i at the end of period t
model.s = Var(model.tot_items, model.tperiods, within = PositiveReals)

#binary setup decision variables indicating whether the production is setup for item i in period t
model.y = Var(model.tot_items, model.tperiods, within = Binary)

#binary linkage decision variables indicating whether a setup state for product i is carried-over from period t-1 to period t
model.lk = Var(model.tot_items, model.tperiods, within = Binary)

#binary variables indicating whether the production on machine m in period t is limited to at most one product for which no setup has to be performed because the setup state for this product is linked to the preceding and following period
model.z = Var(model.machines, model.tperiods, within = Binary)

#Objective

#least cost production plan
def min_cost(model):
    return sum(sum(model.sc[i]*model.y[i,t] for t in model.tperiods) for i in model.tot_items) + sum(sum(model.hc[i]*model.s[i,t] for t in model.tperiods) for i in model.tot_items)
model.min_cost = Objective(rule = min_cost)

#Constraints

#demand satisfaction for end items
def endp_demand_sat(model, i, t):
    if t == 1:
        return model.x[i,1] == model.ext_dem[i,1] + model.s[i,1]
    else:
        return model.x[i,t] + model.s[i,t-1] == model.ext_dem[i,t] + model.s[i,t] 
model.endp_demand_sat = Constraint(model.endp, model.tperiods, rule = endp_demand_sat)

#demand satisfaction for non-end items
def subassemb_demand_sat(model, i, t):
    if t == 1:
        return model.x[i,1] == sum(model.numb_subassemb_prod[i,j]*model.x[j,1] for j in model.succ[i]) + model.s[i,1]
    else:
        return model.x[i,t] + model.s[i,t-1] == sum(model.numb_subassemb_prod[i,j]*model.x[j,t] for j in model.succ[i]) + model.s[i,t]
model.subassemb_demand_sat = Constraint(model.subassemblies, model.tperiods, rule = subassemb_demand_sat)

#Constraints enforcing production capacity limits
def prod_cap_limits(model, m, t):
    return sum(model.prod_time[i,m]*model.x[i,t] for i in model.tot_items) + sum(model.st[i,m]*model.y[i,t] for i in model.tot_items) <= model.capacity[m,t]
model.prod_cap_limits = Constraint(model.machines, model.tperiods, rule = prod_cap_limits)

def no_carry_over_initially(model, i):
    return model.lk[i,1] == 0
model.no_carry_over_initially = Constraint(model.tot_items, rule = no_carry_over_initially)

#Constraints ensuring that the total amount of production is less than a sufficiently large value and that either a setup or a setup carryover is performed in period t for item i if any demand is satisfied using the corresponding production.
def limited_tot_prod(model, i, t):
    return model.x[i,t] <= model.max_units[i,t]*(model.y[i,t] + model.lk[i,t])
model.limited_tot_prod = Constraint(model.tot_items, model.tperiods, rule = limited_tot_prod)

#These constraints guarantee that at most one setup state can be preserved from one period to the next on each machine
def numb_setups(model, m, t):
    if t != 1:
        return sum(model.lk[i,t] for i in model.subset_machines[m]) <= 1
    else:
        return Constraint.Skip
model.numb_setups = Constraint(model.machines, model.tperiods, rule = numb_setups)

#These constraints make sure that a setup state can be carried over to period t only if either item i is setup in period t-1 or the setup state already has been carried over from period t-2 to t-1
def carry_over_setup(model, i, t):
    if t != 1:
        return model.lk[i,t] <= model.y[i,t-1] + model.lk[i,t-1]
    else:
        return Constraint.Skip
model.carry_over_setup = Constraint(model.tot_items, model.tperiods, rule = carry_over_setup)

#These constraints ensure z[m,t] be equal to 1 if a setup state is preserved both over period t-1 to period t and over period t to period t+1
def tbw(model, t, m, i):
    if t == 3:
        return Constraint.Skip
    else:
        if i in model.subset_machines[m]:
            return model.lk[i,t+1] + model.lk[i,t] <= 1 + model.z[m,t]
        else:
            return Constraint.Skip
model.tbw = Constraint(model.tperiods, model.machines, model.tot_items, rule = tbw)

#The following constraints enforce that the performance of a setup and the preservation of a setup state can not happen simultaneously for a machine within a period
def tbr(model, t, m, i):
    if i in model.subset_machines[m]:
        return model.y[i,t] + model.z[m,t] <= 1
    else:
        return Constraint.Skip
model.tbr = Constraint(model.tperiods, model.machines, model.tot_items, rule = tbr)

#Python script for printing the solution in the terminal
solver = SolverFactory("cbc")
instance = model.create_instance("inventory_and_lot_sizing.dat")
results = solver.solve(instance)

for i in instance.tot_items:
    for t in instance.tperiods:
        if value(instance.x[i,t]) > 0:
            print("Number of units of item %s produced in time period %s is: %d" %(i,t,value(instance.x[i,t])))
for i in instance.tot_items:
    for t in instance.tperiods:
        if value(instance.s[i,t]) > 0:
            print("Inventory of item %s in time period %s is: %d" %(i,t,value(instance.s[i,t])))
for i in instance.tot_items:
    for t in instance.tperiods:
        if value(instance.y[i,t]) > 0:
            print("Production is set up for item %s in time period %s" %(i,t))
for i in instance.tot_items:
    for t in instance.tperiods:
        if value(instance.lk[i,t]) > 0:
            print("A setup state for product %s is carried over from time period %s to time period %s" %(i,t-1,t))
for m in instance.machines:
    for t in instance.tperiods:
        if value(instance.z[m,t]) > 0:
            print("The production on machine %s in period %s is limited to at most one product for which no setup has to be performed because the setup state for this product is linked to the precceding and following period" %(m,t))
print("The minimum production cost is: %f" %value(instance.min_cost))
            
#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.tot_items:
        for t in instance.tperiods:
            if value(instance.x[i,t]) > 0:
                output.write("Number of units of item %s produced in time period %s is: %d\n\n" %(i,t,value(instance.x[i,t])))
    for i in instance.tot_items:
        for t in instance.tperiods:
            if value(instance.s[i,t]) > 0:
                output.write("Inventory of item %s in time period %s is: %d\n\n" %(i,t,value(instance.s[i,t])))
    for i in instance.tot_items:
        for t in instance.tperiods:
            if value(instance.y[i,t]) > 0:
                output.write("Production is set up for item %s in time period %s\n\n" %(i,t))
    for i in instance.tot_items:
        for t in instance.tperiods:
            if value(instance.lk[i,t]) > 0:
                output.write("A setup state for product %s is carried over from time period %s to time period %s\n\n" %(i,t-1,t))
    for m in instance.machines:
        for t in instance.tperiods:
            if value(instance.z[m,t]) > 0:
                output.write("The production on machine %s in period %s is limited to at most one product for which no setup has to be performed because the setup state for this product is linked to the precceding and following period\n\n" %(m,t))
    output.write("The minimum production cost is: %f\n\n" %value(instance.min_cost))
    output.close()
