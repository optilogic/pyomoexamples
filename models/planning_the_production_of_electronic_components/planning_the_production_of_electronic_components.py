# For the next six months the production of four products is to be planned 
# by a company. The demand data per time period, the production and storage 
# costs, and the initial and desired final stock levels for every product 
# are given. The cost incurred is proportional to the increase or reduction 
# of the production compared to the preceding month. The cost for an increase 
# of the production is $1 per unit but only $0.50 for a decrease of the production 
# level. The problem is to determine what is the production plan that minimizes 
# the sum of costs incurred through changes of the production level, production 
# and storage costs.

from pyomo.environ import *

model = AbstractModel("Planning the production of electronic components")

#Sets and parameters

#set of components
model.components = Set()

#set of planning periods
model.planning_periods = Set()

#demand of a product in a time period
model.demand = Param(model.components, model.planning_periods, within = NonNegativeIntegers)

#cost of producing one unit of a product
model.prod_cost = Param(model.components, within = NonNegativeReals)

#cost of storing one unit of a product
model.store_cost = Param(model.components, within = NonNegativeReals)

#initial stock level of product p
model.istock = Param(model.components, within = NonNegativeReals)

#final stock level of product p
model.fstock = Param(model.components, within = NonNegativeReals)

#cost of increasing the production
model.cost_inc = Param(within = NonNegativeReals)

#cost of decreasing the production
model.cost_dec = Param(within = NonNegativeReals)

#Variables

#quantity of product p produced at time t
model.produce = Var(model.components, model.planning_periods, within = NonNegativeReals)

#quantity of product p stored at time t
model.store = Var(model.components, model.planning_periods, within = NonNegativeReals)

#measures the reduction to the level of production
model.reduction = Var(model.planning_periods, within = NonNegativeReals)

#measures the increase to the level of production
model.increase = Var(model.planning_periods, within = NonNegativeReals)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(model.prod_cost[p]*model.produce[p,t] + model.store_cost[p]*model.store[p,t] for p in model.components for t in model.planning_periods) + sum(model.cost_inc*model.increase[t] + model.cost_dec*model.reduction[t] for t in model.planning_periods if t != 1)
model.min_cost = Objective(rule = min_cost)

#Constraints

#stock balance for each time period and each component
def stock_balance(model, p, t):
    if t == 1:
        return model.store[p,1] == model.istock[p] + model.produce[p,1] - model.demand[p,1]
    else:
        return model.store[p,t] == model.store[p,t-1] + model.produce[p,t] - model.demand[p,t]
model.stock_balance = Constraint(model.components, model.planning_periods, rule = stock_balance)

#the amount stored in the last time period must be at least as much as the final stock 
def final_stock_bound(model, p):
    return model.store[p,6] >= model.fstock[p]
model.final_stock_bound = Constraint(model.components, rule = final_stock_bound)

#Change in the level of production for each time period
def level_prod_change(model, t):
    if t == 1:
        return Constraint.Skip
    else:
        return sum(model.produce[p,t] for p in model.components) - sum(model.produce[p,t-1] for p in model.components) == model.increase[t] - model.reduction[t]
model.level_prod_change = Constraint(model.planning_periods, rule = level_prod_change)


solver = SolverFactory("cbc")
instance = model.create_instance("planning_the_production_of_electronic_components.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.components:
    for j in instance.planning_periods:
        if value(instance.produce[i,j]) > 0:
            print(f'{value(instance.produce[i,j])} units of product {i} will be produced at time period {j}')
for i in instance.components:
    for j in instance.planning_periods:
        if value(instance.store[i,j]) > 0:
            print(f'{value(instance.produce[i,j])} units of product {i} will be stored at time period {j}')
for i in instance.planning_periods:
    if i != 1 and value(instance.reduction[i]) > 0:
        print(f'The level of production at time period {i} will reduced by {value(instance.reduction[i])}')
for i in instance.planning_periods:
    if i != 1 and value(instance.increase[i]) > 0:
        print(f'The level of production at time period {i} will increased by {value(instance.increase[i])}')
print(f'The minimum total cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.components:
        for j in instance.planning_periods:
            if value(instance.produce[i,j]) > 0:
                output.write(f'{value(instance.produce[i,j])} units of product {i} will be produced at time period {j}\n\n')
    for i in instance.components:
        for j in instance.planning_periods:
            if value(instance.store[i,j]) > 0:
                output.write(f'{value(instance.produce[i,j])} units of product {i} will be stored at time period {j}\n\n')
    for i in instance.planning_periods:
        if i != 1 and value(instance.reduction[i]) > 0:
            output.write(f'The level of production at time period {i} will reduced by {value(instance.reduction[i])}\n\n')
    for i in instance.planning_periods:
        if i != 1 and value(instance.increase[i]) > 0:
            output.write(f'The level of production at time period {i} will increased by {value(instance.increase[i])}\n\n')
    output.write(f'The minimum total cost is {value(instance.min_cost)}')
    output.close()
