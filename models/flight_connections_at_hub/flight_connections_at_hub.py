# An airline uses an European airport as a hub so as to minimize the flight connections to 
# European destinations. Six airplanes of this airline from different cities are landing within 
# a time period. The same aircrafts are taking off to some other destinations within another 
# time period. The numbers of passengers transferring from the incoming flights to one of the 
# outgoing flights are given. The problem is to determine how should the arriving planes be 
# re-used for the departing flights to minimize the number of passengers who have to change planes at the hub.

from pyomo.environ import *

model = AbstractModel("Flight connections at a hub")

#Sets and parameters

#set of planes
model.planes = Set()

#set of origins
model.origins = Set()

#set of destinations
model.destinations = Set()

#set of arcs connecting origins with destinations
def arcs(model):
    return ((i,j) for i in model.origins for j in model.destinations)
model.origin_destination = Set(dimen = 2, initialize = arcs)

#number of passengers transferring from an origin to a destination
model.passengers = Param(model.origin_destination, within = NonNegativeIntegers)

#Variables

#binary variable x which is 1 if and only if plane coming from origin i continuous flying to origin j
model.x = Var(model.origin_destination, within = Binary)

#Objective

#maximizing the number of passengers staying on board their plane at the hub
def max_passengers_stay_board(model):
    return sum(sum(model.passengers[i,j]*model.x[i,j] for j in model.planes) for i in model.planes)
model.max_passengers_stay_board = Objective(rule = max_passengers_stay_board, sense = maximize)

#Constraints

#every destination is served by exactly one flight
def one_flight_destination(model, j):
    return sum(model.x[i,j] for i in model.planes) == 1
model.one_flight_destination = Constraint(model.planes, rule = one_flight_destination)

#one and only one flight leaves every origin
def one_flight_origin(model, i):
    return sum(model.x[i,j] for j in model.planes) == 1
model.one_flight_origin = Constraint(model.planes, rule = one_flight_origin)

solver = SolverFactory("cbc")
instance = model.create_instance("flight_connections_at_hub.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for (i,j) in instance.origin_destination:
    if value(instance.x[i,j]) > 0:
        print(f'Plane coming from origin {i} continuous flying to origin {j}')
print(f'The maximum number of passengers staying on board of their plane at a hub is {value(instance.max_passengers_stay_board)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in instance.origin_destination:
        if value(instance.x[i,j]) > 0:
            output.write(f'Plane coming from origin {i} continuous flying to origin {j}\n\n')
    output.write(f'The maximum number of passengers staying on board of their plane at a hub is {value(instance.max_passengers_stay_board)}')
    output.close()
