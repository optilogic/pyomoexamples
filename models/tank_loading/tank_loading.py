# Five tanker ships have arrived at a chemical factory. They are carrying loads of liquid 
# products that must not be mixed: 1200 tonnes of Benzol, 700 tonnes of Butanol, 1000 tonnes 
# of Propanol, 450 tonnes of Styrene, and 1200 tonnes of THF. Nine tanks of different 
# capacities are available on site. Some of them are already partially filled with a liquid. 
# The capacity of each tank and the initial quantity of certain liquids in certain tanks are given. 
# The problem is to determine into which tanks should the ships be unloaded to maximize the capacity 
# of the tanks that remain unused.

from pyomo.environ import *

model = AbstractModel("Tank loading")

#Sets and parameters

#set of tanks
model.tanks = Set()

#set of tanks with initial quantity zero
model.tanks0 = Set()

#set of tanks with initial quantity > 0
model.tankspositive = Set()

#set of liquids
model.liquids = Set()

#amount of each liquid type
model.liquid_quantity = Param(model.liquids, within = NonNegativeReals)

#capacity of each tank
model.cap_tank = Param(model.tanks, within = NonNegativeIntegers)

#initial quantity of a liquid in a tank (if positive)
model.init_quant = Param(model.tanks, model.liquids, within = NonNegativeReals)

#rest of liquids
model.remained_amount = Param(model.liquids, within = NonNegativeReals)

#Variables

#binary variable which is 1 if liquid l is unloaded from tank t
model.unload = Var(model.liquids, model.tanks0, within = Binary)

#Objective

#minimizing the total capacity of the tanks that are used
def min_total_cap(model):
    return sum(sum(model.cap_tank[t]*model.unload[l,t] for t in model.tanks0) for l in model.liquids) + sum(model.cap_tank[t] for t in model.tankspositive)
model.min_total_cap = Objective(rule = min_total_cap)

#Constraints

#the set of tanks that are filled with the liquid l have a sufficiently large total capacity
def enough_cap(model, l):
    return sum(model.cap_tank[t]*model.unload[l,t] for t in model.tanks0) >= model.remained_amount[l]
model.enough_cap = Constraint(model.liquids, rule = enough_cap)

#every empty tank is filled with at most one liquid
def liq_empty_tank(model, t):
    return sum(model.unload[l,t] for l in model.liquids) <= 1
model.liq_empty_tank = Constraint(model.tanks0, rule = liq_empty_tank)


solver = SolverFactory("cbc")
instance = model.create_instance("tank_loading.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.liquids:
    for j in instance.tanks0:
        if value(instance.unload[i,j]) > 0:
            print(f'Liquid {i} is unloaded from tank {j}')
print(f'The minimum of the total capacity of the tanks that are used is {value(instance.min_total_cap)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.liquids:
        for j in instance.tanks0:
            if value(instance.unload[i,j]) > 0:
                output.write(f'Liquid {i} is unloaded from tank {j}\n\n')
    output.write(f'The minimum of the total capacity of the tanks that are used is {value(instance.min_total_cap)}')
    output.close()
