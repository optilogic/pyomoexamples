# As a part of its weekly production a paint company produces five batches of paints, always 
# the same, for some big clients who have a stable demand. Every paint batch is produced in 
# a single production process, all in the same blender that needs to be cleaned between two 
# batches. The durations of blending paint batches 1 to 5 are respectively 40, 35, 45, 32, and 
# 50 minutes. The cleaning times depend on the colors and the paint types. The problem is to 
# determine which is the corresponding order of paint batches.

from pyomo.environ import *

model = AbstractModel("Paint production")

#Sets and parameters

#set of paint batches
model.paint_batches = Set()

#duration of blending paint batches
model.duration = Param(model.paint_batches, within = NonNegativeIntegers)

#cleaning time between batch i and batch j
model.clean_time = Param(model.paint_batches, model.paint_batches, within = NonNegativeIntegers)

#Variables

#binary variable indication whether batch j succeeds batch i
model.succ = Var(model.paint_batches, model.paint_batches, within = Binary)

#auxiliary variable for subtour elimination constraints 
model.y = Var(model.paint_batches, within = NonNegativeReals)

#Objective

#minimizing the total time
def min_time(model):
    x=0
    for i in model.paint_batches:
        for j in model.paint_batches:
            if i == j:
                Objective.Skip
            else:
                x += (model.duration[i]+model.clean_time[i,j])*model.succ[i,j]
    return x
model.min_time = Objective(rule = min_time)

#Constraints

#every batch has a single successor
def single_successor(model, i):
    return sum(model.succ[i,j] for j in model.paint_batches if j is not i) == 1
model.single_successor = Constraint(model.paint_batches, rule = single_successor)

#every batch has a single predecessor
def single_predecessor(model, j):
    return sum(model.succ[i,j] for i in model.paint_batches if i is not j) == 1
model.single_predecessor = Constraint(model.paint_batches, rule = single_predecessor)

#subtour elimination constraints
def no_sub_cycle(model, i, j):
    if i == j:
        return Constraint.Skip
    elif j == 1:
        return Constraint.Skip
    else:
        return model.y[j] >= model.y[i] + 1 - 5 * (1- model.succ[i,j])
model.no_sub_cycle = Constraint(model.paint_batches, model.paint_batches, rule = no_sub_cycle)


solver = SolverFactory("cbc")
instance = model.create_instance("paint_production.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.paint_batches:
    for j in instance.paint_batches:
        if i != j and value(instance.succ[i,j]) > 0:
            print(f'Batch {j} succeeds batch {i}')
print(f'The minimum of the total time is {value(instance.min_time)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.paint_batches:
        for j in instance.paint_batches:
            if i != j and value(instance.succ[i,j]) > 0:
                output.write(f'Batch {j} succeeds batch {i}\n\n')
    output.write(f'The minimum of the total time is {value(instance.min_time)}')
    output.close()
