# A number of employees plan to retire over a period of seven years. A bank 
# establishes an early retirement scheme. In order to finance the early retirement 
# scheme the bank decides to invest in bonds over the next years. The amount of money 
# to cover the pre-retirement leavers is given for each year. There are available 
# three type of bonds and their value, interest rate and duration are given. The bank 
# decides to buy bonds at the beginning of the first year but not in the following years. 
# The problem is to determine the investments for each bond in order to spend the least 
# amount of money to cover the projected retirement plan.

from pyomo.environ import *

model = AbstractModel("Financing an early retirement scheme")

#Sets and parameters

#set of years over which the early retirement scheme stretches
model.years = Set()

#set of loans
model.loans = Set()

#necessary amounts to cover the pre-retirement leavers
model.amounts = Param(model.years, within = NonNegativeIntegers)

#rates per loan
model.rates = Param(model.loans, within = NonNegativeReals)

#duration of the bonds
model.years_per_loan = Param(model.loans, within = NonNegativeIntegers)

#value of bonds per loan
model.value = Param(model.loans, within = NonNegativeReals)

#interest rate for the money that is not invested in bonds
model.interest = Param(within = NonNegativeReals)

#Variables

#variable representing the number of bonds of each type bought in the first year
model.buy = Var(model.loans, within = NonNegativeIntegers)

#variable representing the sum invested in each year
model.invest = Var(model.years, within = NonNegativeReals)

#variable representing the total capital invested
model.capital = Var(within = NonNegativeReals)

#Objective

#minimizing the total invested capital
def min_cap(model):
    return model.capital
model.min_cap = Objective(rule = min_cap)

#Constraints

#the investment in the first year, during which the bonds are bought
def invest_year1(model):
    return model.capital - sum(model.value[l]*model.buy[l] for l in model.loans) - model.invest[1] == model.amounts[1]
model.invest_year1 = Constraint(rule = invest_year1)

#equilibrium constraints for years 2,3 and 4
def equilib234(model, t):
    if t not in (2,3,4):
        return Constraint.Skip
    else:
        return sum(model.value[l]*model.buy[l]*model.rates[l] for l in model.loans) + (1 + model.interest)*model.invest[t-1] - model.invest[t] == model.amounts[t]
model.equilib234 = Constraint(model.years, rule = equilib234)

#equilibrium constraints for years 5 and 6
def equilib56(model, t):
    if t not in (5,6):
        return Constraint.Skip
    else:
        return sum(model.value[l]*model.buy[l]*(1 + model.rates[l]) for l in model.loans if model.years_per_loan[l] == t - 1) + sum(model.value[l]*model.buy[l]*model.rates[l] for l in model.loans if model.years_per_loan[l] >= t) + (1 + model.interest)*model.invest[t-1] - model.invest[t] == model.amounts[t]
model.equilib56 = Constraint(model.years, rule = equilib56)

#equilibrium constraint for year 7 
def equilib7(model):
    return sum(model.value[l]*model.buy[l]*(1 + model.rates[l]) for l in model.loans if model.years_per_loan[l] == 6) + (1 + model.interest)*model.invest[6] == model.amounts[7]
model.equilib7 = Constraint(rule = equilib7)


solver = SolverFactory("cbc")
instance = model.create_instance("financing_an_early_retirement_scheme.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.loans:
        if value(instance.buy[i]) > 0:
            print(f'The number of bonds bought for each type in the first year is {value(instance.buy[i])}')
for i in instance.years:
    if i != 7 and value(instance.invest[i]) > 0:
        print(f'The amount invested in year {i} is {value(instance.invest[i])}')
print(f'The total capital invested is {value(instance.min_cap)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.loans:
        if value(instance.buy[i]) > 0:
            output.write(f'The number of bonds bought for each type in the first year is {value(instance.buy[i])}\n\n')
    for i in instance.years:
        if i != 7 and value(instance.invest[i]) > 0:
            output.write(f'The amount invested in year i is {value(instance.invest[i])}\n\n')
    output.write(f'The total capital invested is {value(instance.min_cap)}')
    output.close()
