# Usually, in mixed-integer linear programming formulations there are
# constraints of the type sum_{i in I} a_{i} x_{i}, where
# x_{i} in {0,1} and a_i in Z, also referred to as
# knapsack constraint. In order to facilitate the solving process of
# such formulations, a pre-solving phase is applied to reduce the size
# of the model, and more importantly to "strengthen" the
# formulation. During presolve phase, kanpasack constraints can be
# converted to an equivalent constraint with smaller coefficients. In
# this case, equivalent means another constraints that does not cut off
# any of the feasible solutions to the original problem.

# In our case we consider constraint,
# 9 x_{1} + 13 x_{2} - 14 x_{3} + 17 x_{4} + 13 x_{5} - 19 x_{6} + 23 x_{7} +
# 21 x_{8} <= 37.

from pyomo.environ import *

model = AbstractModel("Optimizng a constraint")

#Sets and parameters

#set of coefficient indices
model.indices = Set()

#sets of ceilings
model.ceilings = Set(list(range(1,7)), within = model.indices)

#sets of roofs
model.roofs = Set(list(range(1,7)), within = model.indices)

#Variables

#variables representing the coefficients of the optimized constraint
model.coeffs = Var(model.indices, within = NonNegativeIntegers)

#Objective

#minimizing the right hand side of the constraint; third and fifth coefficients are negative in the original constraint
def min_rhs(model):
    return model.coeffs[0] - model.coeffs[3] - model.coeffs[5]
model.min_rhs = Objective(rule = min_rhs)

#Constraints

#constraints corresponding to each ceiling
def ceil(model, j):
    return sum(model.coeffs[i] for i in model.ceilings[j]) <= model.coeffs[0]
model.ceil = Constraint(list(range(1,7)), rule = ceil)

#constraints corresponding to each roof
def roof(model, j):
    return sum(model.coeffs[i] for i in model.roofs[j]) >= model.coeffs[0] + 1
model.roof = Constraint(list(range(1,7)), rule = roof)

#constraints ordering coefficients in non-increasing order
def ordering(model, i):
    return model.coeffs[i] >= model.coeffs[i + 1]
model.ordering = Constraint(list(range(1,8)), rule = ordering)


#Python script for printing the solution in the terminal
solver = SolverFactory("cbc")
instance = model.create_instance("optimizing_a_constraint.dat")
results = solver.solve(instance)
print("The coefficients of the optimized constraint are: ")
for i in instance.indices:
    if value(instance.coeffs[i]) > 0:
        print("Coefficient corresponding to variable %s is %d" %(i,value(instance.coeffs[i])))
print("The optimal objective value is: %d" %value(instance.min_rhs))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("The coefficients of the optimized constraint are: \n\n")
    for i in instance.indices:
        if value(instance.coeffs[i]) > 0:
            output.write("Coefficient corresponding to variable %s is %d\n\n" %(i,value(instance.coeffs[i])))
    output.write("The optimal objective value is: %d" %value(instance.min_rhs))
    output.close()
