# Four real and a fictious hospital are considered. Also, resource and service 
# indicators are given for each input and output type. The given data has been 
# analyzed for a period over two years.

from pyomo.environ import *

model = AbstractModel("Efficiency of hospitals")

#Sets and parameters

#set of hospitals
model.hospitals = Set()

#set of services
model.services = Set()

#set of resources
model.resources = Set()

#set of arcs connecting resources to hospitals
def arcs(model):
    return ((i,j) for i in model.resources for j in model.hospitals)
model.resource_hospital = Set(dimen = 2, initialize = arcs)

#resource availabilty in each hospital
model.indicator_resource = Param(model.resource_hospital, within = NonNegativeReals)

#set of arcs connecting services to hospitals
def arcs1(model):
    return ((i,j) for i in model.services for j in model.hospitals)
model.service_hospital = Set(dimen = 2, initialize = arcs1)

#service in each hospital 
model.indicator_service = Param(model.service_hospital, within = NonNegativeReals)

#Variables

#data envelopment analysis indicator
model.coeff = Var(model.hospitals, within = NonNegativeReals)

#service indicators of the fictitious hospital
model.fserv = Var(model.services, within = NonNegativeReals)

#resource indicators of the fictitious hospital
model.fres = Var(model.resources, within = NonNegativeReals)

#efficiency coefficient
model.eff = Var(within = NonNegativeReals)

#Objective

#minimizes the resources required by the fictitious hospital
def min_resources(model):
    return model.eff
model.min_resources = Objective(rule = min_resources)

#Constraints

#sums weighted by the coefficients coeff_i for every service
def weighted_sum_service(model, s):
    return sum(model.indicator_service[s,j]*model.coeff[j] for j in model.hospitals) == model.fserv[s]
model.weighted_sum_service = Constraint(model.services, rule = weighted_sum_service)

#sums weighted by the coefficients coeff_i for every resource
def weighted_sum_resource(model, r):
    return sum(model.indicator_resource[r,j]*model.coeff[j] for j in model.hospitals) == model.fres[r]
model.weighted_sum_resource = Constraint(model.resources, rule = weighted_sum_resource)

#sum of coefficients over all hospitals must be 1
def one_nnz_coeff(model):
    return sum(model.coeff[i] for i in model.hospitals) == 1
model.one_nnz_coeff = Constraint(rule = one_nnz_coeff)

#The service indicators of the fictitious hospital must be larger than the service indicators of the hospital h for which we wish to evaluate the performance
def fictious_larger(model, s):
    return model.fserv[s] >= model.indicator_service[s,2]
model.fictious_larger = Constraint(model.services, rule = fictious_larger)

#The hospital 2 uses less resources than the fictitious hospital
def fictious_more_resources(model, r):
    return model.fres[r] <= model.indicator_resource[r,2] * model.eff
model.fictious_more_resources = Constraint(model.resources, rule = fictious_more_resources)

solver = SolverFactory("cbc")
instance = model.create_instance("efficiency_of_hospitals.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.hospitals:
    if value(instance.coeff[i]) > 0:
        print("The data envelopment analysis coefficient for hospital %s is %f" %(i,value(instance.coeff[i])))
for i in instance.services:
    if value(instance.fserv[i]) > 0:
            print("Indicator of service %s of the fictitious hospital is: %f" %(i,value(instance.fserv[i])))
for i in instance.resources:
    if value(instance.fres[i]) > 0:
        print("Indicator of resource %s of the fictitious hospital is: %f" %(i,value(instance.fres[i])))
print("The minimum amount of resources required by the fictitious hospital is: %f" %value(instance.min_resources))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.hospitals:
        if value(instance.coeff[i]) > 0:
            output.write("The data envelopment analysis coefficient for hospital %s is %f\n\n" %(i,value(instance.coeff[i])))
    for i in instance.services:
        if value(instance.fserv[i]) > 0:
            output.write("Indicator of service %s of the fictitious hospital is: %f\n\n" %(i,value(instance.fserv[i])))
    for i in instance.resources:
        if value(instance.fres[i]) > 0:
            output.write("Indicator of resource %s of the fictitious hospital is: %f\n\n" %(i,value(instance.fres[i])))
    output.write("The minimum amount of resources required by the fictitious hospital is: %f" %value(instance.min_resources))
    output.close()
