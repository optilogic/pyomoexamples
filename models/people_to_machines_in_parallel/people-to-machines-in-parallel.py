# A set of six workers and a set of six machines along with the productivity of each worker 
# to each machine are given. We wish to maximize the overall productivity by assigning 
# workers to machines in parallel.

from pyomo.environ import *

model = AbstractModel("Scheduling people to machines")

#Sets and parameters

#set of workers
model.workers = RangeSet(1,6)

#set of machines
model.machines = RangeSet(1,6)

#productivity in pieces per hour when worker i is assgned at machine j
model.productivity = Param(model.workers, model.machines, within = NonNegativeIntegers)

#Variables

#binary variable x which is one if and only if person i is assigned to machine j
model.x = Var(model.workers, model.machines, within = Binary)

#Objective

#maximizing the total productivity
def max_productivity(model):
    return sum(sum(model.productivity[i,j]*model.x[i,j] for j in model.machines) for i in model.workers)
model.max_productivity = Objective(rule = max_productivity, sense = maximize)

#Constraints

#For every machine there is exactly one person assigned
def machine2person(model,j):
    return sum(model.x[i,j] for i in model.workers) == 1
model.machine2person = Constraint(model.machines, rule = machine2person)

#Foe every person there is exactly one machine assigned
def person2machine(model, i):
    return sum(model.x[i,j] for j in model.machines) == 1
model.person2machine = Constraint(model.workers, rule = person2machine)

#if there are more workers than machines, then the last constraint can be replaced by:
#def person2machine(model, i):
#    return sum(model.x_ij[i,j] for j in model.machines) <= 1
#model.person2machine = Constraint(model.workers, rule = person2machine)


solver = SolverFactory('cbc')
instance = model.create_instance('people-to-machines-in-parallel.dat')
results = solver.solve(instance)


#Python Script for printing the solution in the terminal
for i in instance.workers:
    for j in instance.machines:
        if value(instance.x[i,j]) > 0:
            print(f'Worker {i} is assigned to machine {j}')
print(f'The maximum of total productivity is {value(instance.max_productivity)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.workers:
        for j in instance.machines:
            if value(instance.x[i,j]) > 0:
                output.write(f'Worker {i} is assigned to machine {j}\n\n')
    output.write(f'The maximum of total productivity is {value(instance.max_productivity)}')
    output.close()
