# A company has received an order for three types of wallpapers: one (paper 1) 
# has a blue background with yellow patterns, another (paper 2) a green background 
# and blue and yellow patterns, and the last (paper 3) has a yellow background 
# with blue and green patterns. Every paper type is produced as a continuous roll 
# of paper that passes through several machines, each printing a different color. 
# The order in which the papers are run through the machines depends on the design 
# of the paper: for paper 1 first the blue background and then the yellow pattern 
# is printed. After the green background for paper 2, first the blue and then the 
# yellow patterns are printed. The printing of paper 3 starts with the yellow 
# background, followed by the blue and then the green patterns. The times (in minutes) 
# for applying every color of the three paper types are given. The problem is to 
# determine how should the paper printing be scheduled on the machines in order to 
# finish the order as early as possible.

from pyomo.environ import *

model=AbstractModel("Job shop scheduling")

#Sets and parameters

#set of tasks
model.tasks = Set()

#duration of each job at each machine
model.duration = Param(model.tasks, within = NonNegativeIntegers)

#set of arcs
model.arcs = Set(within = model.tasks*model.tasks)

#set of disjuctions ( which job should be processed first at a machine)
model.disjunctions = Set(within = model.tasks*model.tasks)

#big-M value
model.M = Param(within = NonNegativeReals)

#Variables

#variable indicating the start of job j 
model.start = Var(model.tasks, within = NonNegativeIntegers)

#variable indicating the total time to finish
model.finish = Var(within = NonNegativeIntegers)

#binary variable indicating the validity of a disjunction
model.disjunc = Var(model.disjunctions, within = Binary)

#Objective

#minimizing the total time
def min_total_time(model):
    return model.finish
model.min_total_time = Objective(rule = min_total_time)

#Constraints

#the completion time of the schedule is greater than or equal to the completion time of the last operation for every paper time, and hence to the completion times of all operations
def lb_finish_time(model, j):
    return model.finish >= model.start[j] + model.duration[j]
model.lb_finish_time = Constraint(model.tasks, rule = lb_finish_time)

#conjuction constraints
def conjunction_constraint(model, i, j):
    return model.start[j] >= model.start[i] + model.duration[i]
model.conjunction_constraint = Constraint(model.arcs, rule = conjunction_constraint)

#disjunction constraints
def disjunctions1(model, i, j):
    return model.start[i] + model.duration[i] <= model.start[j] + model.M*model.disjunc[i,j]
model.disjunctions1 = Constraint(model.disjunctions, rule = disjunctions1)

#disjunction constraints
def disjunctions2(model, i, j):
    return model.start[j] + model.duration[j] <= model.start[i] + model.M*(1 - model.disjunc[i,j])
model.disjunctions2 = Constraint(model.disjunctions, rule = disjunctions2)


solver = SolverFactory("cbc")
instance = model.create_instance("job_shop_scheduling.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.tasks:
    if value(instance.start[i]) > 0:
        print(f'Job {i} will start after {value(instance.start[i])} minutes')
    else:
        print(f'Job {i} will start at the beginning of the time horizon')
print(f'The minimum time to finish all jobs is {value(instance.finish)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.tasks:
        if value(instance.start[i]) > 0:
            output.write(f'Job {i} will start after {value(instance.start[i])} minutes\n\n')
        else:
            output.write(f'Job {i} will start at the beginning of the time horizon\n\n')
    output.write(f'The minimum time to finish all jobs is {value(instance.finish)}')
    output.close()
