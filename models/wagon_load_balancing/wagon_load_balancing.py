# Three railway wagons with a carrying capacity of 100 quintals
# have been reserved to transport sixteen boxes. The weight of 
# the boxes in quintals are given. The problem is to determine 
# how shall the boxes be assigned to the wagons in order to keep 
# to the limits on the maximum carrying capacity and to minimize 
# the heaviest wagon load. 

from pyomo.environ import *
import math

model = AbstractModel("Wagon load balancing")

#Sets and parameters

#set of wagons
model.wagons = Set()

#set of boxes
model.boxes = Set()

#weight of each box
model.weight_box = Param(model.boxes, within = NonNegativeIntegers)

#maximum weight a wagon can carry
model.max_weight_wagon = Param(within = NonNegativeIntegers)

#Variables

#binary variable indicating whether a box will load in a wagon
model.load_bw = Var(model.boxes, model.wagons, within = Binary)

#maximum load of the wagons
model.maxweight = Var(within = NonNegativeReals)

#Objective

#minimizing the maximum load of the wagons
def minmax(model):
    return model.maxweight
model.minmax = Objective(rule = minmax)

#Constraints

#each box must load in exactly one wagon
def each_box_one_wagon(model, b):
    return sum(model.load_bw[b,w] for w in model.wagons) == 1
model.each_box_one_wagon = Constraint(model.boxes, rule = each_box_one_wagon)

#maxweight is the upper bound on the wagon loads
def maxweight_wagon(model, w):
    return sum(model.weight_box[b]*model.load_bw[b,w] for b in model.boxes) <= model.maxweight
model.maxweight_wagon = Constraint(model.wagons, rule = maxweight_wagon)

#upper bound on the maximum load of the wagons
def upper_bound(model):
    return model.max_weight_wagon >= model.maxweight
model.upper_bound = Constraint(rule = upper_bound)

#lower bound on the maximum load of the wagons
def lower_bound(model):
    return math.ceil(sum(model.weight_box[b] for b in model.boxes) / 3) <= model.maxweight
model.lower_bound = Constraint(rule = lower_bound)


solver = SolverFactory("cbc")
instance = model.create_instance("wagon_load_balancing.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.boxes:
    for j in instance.wagons:
        if value(instance.load_bw[i,j]) > 0:
            print(f'Box {i} will load in wagon {j}')
print(f'The minimum of the maximum load of the wagons is {value(instance.minmax)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.boxes:
        for j in instance.wagons:
            if value(instance.load_bw[i,j]) > 0:
                output.write(f'Box {i} will load in wagon {j}\n\n')
    output.write(f'The minimum of the maximum load of the wagons is {value(instance.minmax)}')
    output.close()
