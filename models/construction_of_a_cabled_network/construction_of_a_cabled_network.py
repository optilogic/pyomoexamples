# Six terminals at a university are wished to be connected through a cabled network. 
# The distances between every two different terminals are given. It is assumed that 
# the cost of connection between any two terminals is proportional to the distance. 
# The problem is to determine the connections to install in order to minimize the total cost.

from pyomo.environ import *

model = AbstractModel("Construction of a cabled network")

#Sets and parameters

#set of buildings
model.buildings = Set()

#number of buildings
model.numb_buildings = Param(within = NonNegativeIntegers)

#set of arcs between any pair of buildings
def arcs(model):
    return ((i,j) for i in model.buildings for j in model.buildings)
model.arcs = Set(dimen = 2, initialize = arcs)

#distances between any two buildings
model.distances = Param(model.arcs, within = NonNegativeIntegers)

#Variables

#binary variable indicating whether two buildings are directly connected
model.connected = Var(model.arcs, within = Binary)

#integer variable indicating the length of path from a root to a node
model.level = Var(model.buildings, within = NonNegativeIntegers)

#Objective

#minimizing the total cost of connection which is proportional to the distance
def min_dist(model):
    return sum(sum(model.distances[b1, b2]*model.connected[b1, b2] for b2 in model.buildings if (b1, b2) in model.arcs) for b1 in model.buildings)
model.min_dist = Objective(rule = min_dist)

#Constraints

#constraints indicating a connected graph connecting the 6 buldings
def tree_connection(model):
    return sum(sum(model.connected[b1, b2] for b2 in model.buildings) for b1 in model.buildings) <= model.numb_buildings - 1
model.tree_connection = Constraint(rule = tree_connection)

#directed cycle prevention
def no_dir_cycle(model, b1, b2):
    if b1 == b2:
        return Constraint.Skip
    else:
        return model.level[b2] >= model.level[b1] + 1 - model.numb_buildings + model.numb_buildings*model.connected[b1, b2] 
model.no_dir_cycle = Constraint(model.buildings, model.buildings, rule = no_dir_cycle)

#undirected cycle prevention
def no_undir_cycle(model, b1, b2):
    if b1 == 1:
        return Constraint.Skip
    else:
        return sum(model.connected[b1, b2] for b2 in model.buildings if b2 != b1) == 1
model.no_undir_cycle = Constraint(model.buildings, model.buildings, rule = no_undir_cycle)


solver = SolverFactory("cbc")
instance = model.create_instance("construction_of_a_cabled_network.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for (i,j) in instance.arcs:
    if value(instance.connected[i,j]) > 0:
        print(f'Building {i} is directly connected to building {j}')
for i in instance.buildings:
    if value(instance.level[i]) > 0:
        print(f'Length of path from the root to building {i} is {value(instance.level[i])}')
print(f'The minimum total cost of connection is {value(instance.min_dist)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in instance.arcs:
        if value(instance.connected[i,j]) > 0:
            output.write(f'Building {i} is directly connected to building {j}\n\n')
    for i in instance.buildings:
        if value(instance.level[i]) > 0:
            output.write(f'Length of path from the root to building {i} is {value(instance.level[i])}\n\n')
    output.write(f'The minimum total cost of connection is {value(instance.min_dist)}')
    output.close()
