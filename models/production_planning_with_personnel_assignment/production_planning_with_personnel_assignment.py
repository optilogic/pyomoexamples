# A company wishes to produce 4 type of products into 5 production lines. 
# While satisfying capacity on each production line the company wishes to 
# determine the quantity to produce of each product type by maximizing the total profit.

from pyomo.environ import *

model = AbstractModel("Production planning with personnel assignment")

#Sets and parameters

#set of products
model.products = Set()

#set of production lines
model.lines = Set()

#profit per unit of each product
model.profit = Param(model.products, within = NonNegativeReals)

#the time needed for product p in line l
model.duration = Param(model.products, model.lines, within = NonNegativeReals)

#capacity on each production line
model.capacity = Param(model.lines, within = NonNegativeReals)

#Variables

#integer variable indicating the amount of each product to be produced
model.produce = Var(model.products, within = NonNegativeIntegers)

#Objective

#maximizing the profit
def max_profit(model):
    return sum(model.produce[p]*model.profit[p] for p in model.products)
model.max_profit = Objective(rule = max_profit, sense = maximize)

#Constraints

#capacity limits of every production line
def cap_limit(model, l):
    return sum(model.duration[p,l]*model.produce[p] for p in model.products) <= model.capacity[l]
model.cap_limit = Constraint(model.lines, rule = cap_limit)


solver = SolverFactory("cbc")
instance = model.create_instance("production_planning_with_personnel_assignment.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.products:
    if value(instance.produce[i]) > 0:
        print(f'Product {i} will be produced on the amount of {value(instance.produce[i])}')
print(f'The maximum profit is {value(instance.max_profit)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.products:
        if value(instance.produce[i]) > 0:
            output.write(f'Product {i} will be produced on the amount of {value(instance.produce[i])}\n\n')
    output.write(f'The maximum profit is {value(instance.max_profit)}')
    output.close()
