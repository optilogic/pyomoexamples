# A mobile phone operator decides to equip a currently uncovered geographical zone. 
# The management allocates a budget of $10 million to equip this region. Only 7 
# locations are possible for the construction of the transmitters and it is also 
# known that every transmitter only covers a certain number of communities. 
# Cost and communities covered for every site are given. The problem is to decide 
# where should the transmitters be built to cover the largest population with 
# the given budget limit.

from pyomo.environ import *

model = AbstractModel("Location of GSM transmitters")

#Sets and parameters

#set of possible sites to build a transmitter
model.sites = Set()

#set of communities to be covered
model.communities = Set()

#cost of building a transmitter at each site
model.cost = Param(model.sites, within = NonNegativeReals)

#population of each community to be covered
model.population = Param(model.communities, within = NonNegativeReals)

#set arcs between sites and communities
model.arcs = Set(within = model.sites*model.communities)

#indicator parameter, indicating whether a community is covered by a site in case of having a transmitter build
model.cov = Param(model.arcs, within = Binary)

#budget
model.budget = Param(within = NonNegativeReals)

#Variables

#binary variable indicating whether a community is covered by a transmitter
model.covered = Var(model.communities, within = Binary)

#binary variable indicating whether a transmitter is build at a site
model.build = Var(model.sites, within = Binary)

#Objective

#maximizing the number of people covered
def max_ppl_cov(model):
    return sum(model.population[c]*model.covered[c] for c in model.communities)
model.max_ppl_cov = Objective(rule = max_ppl_cov, sense = maximize)

#Constraints

#if a community c is covered then a transmitter must be build at one of the sites covering c
def comm_site_relation(model, c):
    return sum(model.cov[p,c]*model.build[p] for p in model.sites if (p,c) in model.arcs) >= model.covered[c]
model.comm_site_relation = Constraint(model.communities, rule = comm_site_relation)

#budget limit satisfied
def budget_limit(model):
    return sum(model.cost[p]*model.build[p] for p in model.sites) <= model.budget
model.budget_limit = Constraint(rule = budget_limit)


solver = SolverFactory("cbc")
instance = model.create_instance("location_of_gsm_transmiters.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.communities:
    if value(instance.covered[i]) > 0:
        print(f'Community {i} will be covered by a transmitter ')
for i in instance.sites:
    if value(instance.build[i]) > 0:
        print(f'A transmitter will be build at site {i}')
print(f'The maximum of people who will be covered is {value(instance.max_ppl_cov)} thousands')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.communities:
        if value(instance.covered[i]) > 0:
            output.write(f'Community {i} will be covered by a transmitter\n\n')
    for i in instance.sites:
        if value(instance.build[i]) > 0:
            output.write(f'A transmitter will be build at site {i}\n\n')
    output.write(f'The maximum of people who will be covered is {value(instance.max_ppl_cov)} thousands')
    output.close()
