# A family wishes to determine the allocation of their expenses over one year 
# period of time. There is a fixed living cost, car expenses and rent every 
# month. Also, there is a fixed cost every two months for telephone service. 
# Gas and electricity are paid every six months with a fixed amount, while taxes 
# are paid every 4 months. A monthly payment of $150 of state allowance for families 
# with dependent children and a net salary of $1900 per month is given. There is a 
# least amount spent for leisure every month as well. The problem is to determine 
# how to balance the budget throughout the year in order to maximize the money spent for leisure.

from pyomo.environ import *

model = AbstractModel("Family Budget")

#Sets and parameters

#set of months
model.months = Set()

#set of payment types
model.payments = Set()

#amount of each payment type
model.amount = Param(model.payments, within = NonNegativeReals)

#frequency over which each payment type is made
model.period = Param(model.payments, within = NonNegativeIntegers)

#salary per month
model.income = Param( within = NonNegativeReals )

#allowance
model.allowance = Param( within = NonNegativeReals )

#minimum amount wished to be spent for fun each month
model.min_fun_exp = Param(within = NonNegativeReals)

#Variables

#variable representing the amount of money to be spent for fun on each month
model.fun_expense = Var(model.months, within = NonNegativeReals)

#variable representing the amount of money saved on each month
model.saving = Var(model.months, within = NonNegativeReals)

#Objective

#maximizing the total amount available for leisure in the entire year
def max_fun_exp(model):
    return sum(model.fun_expense[m] for m in model.months)
model.max_fun_exp = Objective(rule = max_fun_exp, sense = maximize)

#Constraints

#minimum amount to be spent for fun each month
def fun_exp(model, m):
    return model.fun_expense[m] >= model.min_fun_exp
model.fun_exp = Constraint(model.months, rule = fun_exp)

#the expenses of the current month plus the savings of this month and the money spent for leisure must be less than or equal to the perceived salary, plus the allowance, plus the savings from the preceding month
def upper_bound_on_expenses_month(model, m):
    if m >= 2:
        return sum(model.amount[i] for i in model.payments if m % model.period[i] == 0) + model.saving[m] + model.fun_expense[m] <= model.income + model.allowance + model.saving[m - 1]
    else:
        return model.amount[1] + model.amount[2] + model.amount[5] + model.saving[1] + model.fun_expense[1] <= model.income + model.allowance
model.upper_bound_on_expenses_month = Constraint(model.months, rule = upper_bound_on_expenses_month)


solver = SolverFactory("cbc")
instance = model.create_instance("family_budget.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.months:
        if value(instance.fun_expense[i]) > 0:
            print(f'The amount of money to be spent for fun during month {i} is {value(instance.fun_expense[i])}')
for i in instance.months:
    if value(instance.saving[i]) > 0:
        print(f'The amount of money saved on month {i} is {value(instance.saving[i])}')
print(f'The maximum amount of money to be spent for fun during the year is {value(instance.max_fun_exp)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.months:
        if value(instance.fun_expense[i]) > 0:
            output.write(f'The amount of money to be spent for fun during month {i} is {value(instance.fun_expense[i])}\n\n')
    for i in instance.months:
        if value(instance.saving[i]) > 0:
            output.write(f'The amount of money saved on month {i} is {value(instance.saving[i])}\n\n')
    output.write(f'The maximum amount of money to be spent for fun during the year is {value(instance.max_fun_exp)}')
    output.close()
