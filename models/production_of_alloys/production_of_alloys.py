# The company Steel has received an order for 500 tonnes of steel 
# to be used in shipbuilding. The characteristics (‘grades’) of the 
# steel are given. The company has seven different raw materials in 
# stock that may be used for the production of this steel. The grades, 
# available amounts and prices for all raw materials are given. The objective 
# is to determine the composition of the steel that minimizes the production cost.

from pyomo.environ import *

model = AbstractModel("Production of alloys")

#Sets and parameters

#set of raw materials
model.raw = Set()

#set of components
model.comp = Set()

#availability of each raw material
model.AVAIL = Param(model.raw)

#grades of each component in each raw material
model.P = Param(model.raw, model.comp, within = NonNegativeReals)

#minimum grade of each component in the ordered steel
model.PMIN = Param(model.comp)

#maximum grade of each component in the ordered steel
model.PMAX = Param(model.comp)

#cost per unit of each raw material
model.COST_r = Param(model.raw)

#amount of steel demanded  by the company
model.DEM = Param(within=NonNegativeReals)

#Variables

#amount used of each raw material
model.x = Var(model.raw ,domain=NonNegativeReals)

#amount of steel produced
model.prod = Var(domain=NonNegativeReals)

#Objective

#minimizing the production cost
def min_cost(model):
    return summation(model.COST_r, model.x)
model.OBJ = Objective(rule=min_cost)

#Constraints

#lower bound on the grade of the final product
def lb_grade(model, j):
    return sum(model.P[i,j] * model.x[i] for i in model.raw) >= model.PMIN[j]*model.prod 
model.lb_grade = Constraint(model.comp, rule= lb_grade)

#upper bound on the grade of the final product
def ub_grade(model,j):
    return sum(model.P[r,j]*model.x[r] for r in model.raw)<=model.PMAX[j]*model.prod
model.ub_grade = Constraint(model.comp, rule= ub_grade)

#limit on the amount of each raw material used
def limit_use_of_raw_material(model, i):
    return  model.x[i]<=model.AVAIL[i]
model.limit_use_of_raw_material = Constraint(model.raw, rule = limit_use_of_raw_material)

#the resulting product weight is equal to the sum of the weights of the raw materials used for its production.
def produced_amount(model):
    return model.prod == sum(model.x[r] for r in model.raw)
model.produced_amount = Constraint(rule = produced_amount)

#the produced amount of steel satisfies the demand
def demand_satisfied(model):
    return model.prod >= model.DEM
model.demand_satisfied = Constraint(rule = demand_satisfied)

solver = SolverFactory("cbc")
instance = model.create_instance("production_of_alloys.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.raw:
    if value(instance.x[i]) > 0:
        print(f'The amount of {i} used is {value(instance.x[i])}')
print(f'The amount of steel produced is {value(instance.prod)}')
print(f'The minimum production cost is {value(instance.OBJ)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.raw:
        if value(instance.x[i]) > 0:
            output.write(f'The amount of {i} used is {value(instance.x[i])}\n\n')
    output.write(f'The amount of steel produced is {value(instance.prod)}\n\n')
    output.write(f'The minimum production cost is {value(instance.OBJ)}')
    output.close()

    




