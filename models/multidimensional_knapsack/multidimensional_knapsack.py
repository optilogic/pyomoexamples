# Consider a set of projects and a set of resources. The number of each resource needed 
# for each project is given. Also, a profit for chossing a project is given. We wish to 
# maximize the total profit by choosing projects that satisfy the availability of resources.

from pyomo.environ import *

model = AbstractModel("Capital Budgeting Problem")

#Sets and parameters

#set of projects
model.projects = Set()

#set of resources
model.resources = Set()

#profit for each project
model.profit = Param(model.projects, within = NonNegativeReals)

#units of resource i needed for project j
model.resource_per_project = Param(model.resources, model.projects, within = NonNegativeReals)

#capacity of each resource
model.capacity = Param(model.resources, within = NonNegativeReals)

#Variables

#binary variable indicating whether to choose a project
model.choose = Var(model.projects, within = Binary)

#Objective

#maximizing the total profit
def max_profit(model):
    return sum(model.profit[j]*model.choose[j] for j in model.projects)
model.max_profit = Objective(rule = max_profit, sense = maximize)

#Constraints

#satisfying capacity of each resource
def cap_satisfied(model, i):
    return sum(model.resource_per_project[i,j]*model.choose[j] for j in model.projects) <= model.capacity[i]
model.cap_satisfied = Constraint(model.resources, rule = cap_satisfied)


#Python script for printing the solution in the terminal
solver = SolverFactory("cbc")
instance = model.create_instance("multidimensional_knapsack.dat")
results = solver.solve(instance)

print("The following projects are chosen:")
for i in instance.projects:
    if value(instance.choose[i]) > 0:
        print("%s " %i)
print("The maximum profit is: %f" %value(instance.max_profit))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("The following projects are chosen:\n")
    for i in instance.projects:
        if value(instance.choose[i]) > 0:
            output.write("%s \n" %i)
    output.write("\nThe maximum profit is: %f" %value(instance.max_profit))
    output.close()
