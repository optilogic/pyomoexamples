# The current head of state wishes to consolidate his position in the capital, 
# the fourteen quarters of which need to be grouped to electoral districts. 
# For each quarter there is a forecast number of favorable votes and the total 
# number of electors. All electors must vote and the winner needs to have the 
# absolute majority. A valid electoral district is formed by several adjacent 
# quarters and must have between 30,000 and 100,000 voters. Electoral districts 
# consisting of a single quarter are permitted if it has at least 50,000 voters. 
# Nevertheless, the head of the state may not decently define an electoral district 
# solely with the quarter in which he resides. The head of the state wishes to determine 
# a partitioning into six electoral districts that maximizes the number of seats.

from pyomo.environ import *

model = AbstractModel("Rigging elections")

#Sets and parameters

#set of quarters
model.quarters = Set()

#the minimum size a district with a single quarter can have
model.single_quarter_district_size = Param(within = NonNegativeIntegers)

#minimum size a district with multiple quarters can have
model.min_district_size = Param(within = NonNegativeIntegers)

#maximum size a district with multiple quarters can have
model.max_district_size = Param(within = NonNegativeIntegers)

#number of electors per quarter
model.electors_quarter = Param(model.quarters, within = NonNegativeIntegers)

#number of voters per quarter
model.voters = Param(model.quarters, within = NonNegativeReals)

#set of all possible districts
model.districts = RangeSet(1,48)

#number of required districts
model.number_required_districts = Param(within = NonNegativeIntegers)

#set of arcs between set of quarters and districts
model.quarter_district = Set(within = model.quarters*model.districts)

#indicating whether for a possible district the party "will win" or not
model.indicator = Param(model.districts, within = Binary)

#Variables

#binary variable indicating whether a district is chosen
model.district = Var(model.districts, within = Binary)

#Objective

#maximizing the number of supporters over all possible district partitioning
def max_support(model):
    return sum(model.indicator[i]*model.district[i] for i in model.districts)
model.max_support = Objective(rule = max_support, sense = maximize)

#Constraints

#exactly m districts are chosen
def number_districts_chosen(model):
    return sum(model.district[i] for i in model.districts) == model.number_required_districts
model.number_districts_chosen = Constraint(rule = number_districts_chosen)

#every quarter is assigned to exactly one district in the chosen partition
def quarter2single_district(model, q):
    return sum(model.district[d] for d in model.districts if (q,d) in model.quarter_district) == 1
model.quarter2single_district = Constraint(model.quarters, rule = quarter2single_district)


solver = SolverFactory("cbc")
instance = model.create_instance("rigging_elections.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.districts:
    if value(instance.district[i]) > 0:
        print("District %s is chosen for partitioning" %i)
print("The maximum number of supporters is: %f" %value(instance.max_support))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.districts:
        if value(instance.district[i]) > 0:
            output.write("District %s is chosen for partitioning\n\n" %i)
    output.write("The maximum number of supporters is: %f" %value(instance.max_support))
    output.close()
