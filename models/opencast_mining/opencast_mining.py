# An opencast uranium mine is being prospected. Based on the results of some 
# test drillings the mine has been subdivided into exploitation units called blocks. 
# 18 blocks of 10,000 tonnes on three levels have been identified. To extract a block, 
# three blocks of the level above it need to be extracted: the block immediately on 
# top of it, and also, due to the constraints on the slope, the blocks to the right and 
# to the left. It costs $100 per tonne to extract a block of level 1, $200 per tonne for 
# a block of level 2, and $300 per tonne for a block of level 3, with the exception of 
# the hatched blocks that are formed of a very hard rock rich in quartz and cost $1000 per 
# ton. The only blocks that contain uranium are those displayed in a gray shade (1, 7, 10, 
# 12, 17, 18). Their market value is 200, 300, 500, 200, 1000, and $1200/tonne respectively. 
# Block 18, although rich in ore, is made of the same hard rock as the other hatched blocks. 
# The problem is to determine which blocks should be extracted to maximize the total benefit.

from pyomo.environ import *

model = AbstractModel("Opencast Mining")

#Sets and parameters

#set of blocks
model.blocks = Set()

#set of blocks in levels 2 and 3
model.blocks_level_2_3 = Set(within = model.blocks)

#set of blocks that need to be exctracted for each block in level 2 and 3
model.extract_block = Set(model.blocks_level_2_3, within = model.blocks)

#value per tonne of a block
model.val = Param(model.blocks, within = NonNegativeReals)

#cost per tonne of extracting a block
model.cost = Param(model.blocks, within = NonNegativeReals)

#weight of each block
model.weight = Param(within = NonNegativeReals)

#Variables

#binary variable deciding whether to extract a block
model.extract = Var(model.blocks, within = Binary)

#Objective

#maximizing the sum of the benefits from the blocks that are extracted
def max_benefit(model):
    return sum((model.val[b] - model.cost[b])*model.extract[b] for b in model.blocks) * model.weight
model.max_benefit = Objective(rule = max_benefit, sense = maximize)

#Constraints

#the blocks are extracted in the right order
def extract_in_order(model, b, a):
    if a in model.extract_block[b]:
        return model.extract[b] <= model.extract[a]
    else:
        return Constraint.Skip
model.extract_in_order = Constraint(model.blocks_level_2_3, model.blocks, rule = extract_in_order)


solver = SolverFactory("cbc")
instance = model.create_instance("opencast_mining.dat")
results = solver.solve(instance)


#Python Script for printing the solution in the terminal
for i in instance.blocks:
    if value(instance.extract[i]) > 0:
        print(f'Block {i} should be extracted')
print(f'The maximum benefit from extracting the blocks is {value(instance.max_benefit)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.blocks:
        if value(instance.extract[i]) > 0:
            output.write(f'Block {i} should be extracted\n\n')
    output.write(f'The maximum benefit from extracting the blocks is {value(instance.max_benefit)}')
    output.close()
