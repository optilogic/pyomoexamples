# A chain of department stores uses a fleet of vans rented from different rental agencies. 
# For the next six months period the demand for vans is given. To satisfy its needs, the 
# chain has a choice among three types of contracts that may start the first day of every 
# month: 3-months contracts for a total cost of $1700 per van, 4-months contracts at $2200 
# per van, and 5-months contracts at $2600 per van. The problem is to determine how many 
# contracts of the different types need to be started every month in order to satisfy the 
# company’s needs at the least cost and to have no remaining vans rented after the end of 
# the time horizon.

from pyomo.environ import *

model = AbstractModel("Fleet planning for vans")

#Sets and parameters

#set of months
model.months = Set()

#requirements on the number of vans for each month
model.requirements = Param(model.months, within = NonNegativeReals)

#initial amount of vans at the beginning of planning
model.initial_number = Param(within = NonNegativeReals)

#set of type of contracts
model.contracts = Set()

#cost of each contract type
model.cost = Param(model.contracts, within = NonNegativeReals)

#Variables

#integer variables x_cm to denote the number of contracts of type c to start at the beginning of month m
model.x_cm = Var(model.contracts, model.months, within = NonNegativeIntegers)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(sum(model.cost[c]*model.x_cm[c,m] for m in model.months) for c in model.contracts)
model.min_cost = Objective(rule = min_cost)

#Constraints

#For months 1 and 2 the signed contracts together with the initial amount of vans have to cover the need for vans of the month; starting from March the signed contracts have to cover the need for vans of the month
def req_satisfied(model, m):
    if m == 1:
        return model.initial_number + sum(model.x_cm[c, m] for c in model.contracts) >= model.requirements[m]
    elif m == 2 :
        return model.initial_number + sum(sum(model.x_cm[c, m] for m in range(1,3)) for c in model.contracts) >= model.requirements[m]
    else:
        return sum(sum(model.x_cm[c, n] for n in range(max(1, m-c+1), min(m,6-c+1) + 1)) for c in model.contracts) >= model.requirements[m]
model.req_satisfied = Constraint(model.months, rule = req_satisfied)


solver = SolverFactory("cbc")
instance = model.create_instance("fleet_planning_for_vans.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.contracts:
    for j in instance.months:
        if value(instance.x_cm[i,j]) > 0:
            print(f'Number of contracts of type {i} to start at the beginning of month {j} is {value(instance.x_cm[i,j])}')
print(f'The minimum total cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.contracts:
        for j in instance.months:
            if value(instance.x_cm[i,j]) > 0:
                output.write(f'Number of contracts of type {i} to start at the beginning of month {j} is {value(instance.x_cm[i,j])}\n\n')
    output.write(f'The minimum total cost is {value(instance.min_cost)}')
    output.close()

