# Given a set of courses at a university, there are obligatory courses that each student has to 
# take during a semester. Also, each student can choose a certain number of elective courses. 
# It is wished to find a schedule of the final exams so that there is no conflict on any students schedule of exams.

from pyomo.environ import *

model = AbstractModel("Exam scheduling")

#Sets and parameters

#set of exams
model.exams = RangeSet(1,11)

#set of arcs between incompatible exams
model.arcs = Set(within = model.exams*model.exams)

#set of time slices
model.time = Set()

#Variables

#binary variables indicating whether an exam is taken on time slice t
model.sched = Var(model.exams, model.time, within = Binary)

#Objective

#no objective function needed for this model as we are interested in only finding a feasible solution
def const_obj(model):
    return 0
model.const_obj = Objective(rule = const_obj)

#Constraints

#every exam needs to be scheduled exactly once
def each_exam_once(model, i):
    return sum(model.sched[i,t] for t in model.time) == 1
model.each_exam_once = Constraint(model.exams, rule = each_exam_once)

#two incompatible exams may not be scheduled at the same time
def sep_incomp_exams(model, i, j, t):
    if (i,j) in model.arcs:
        return model.sched[i,t] + model.sched[j,t] <= 1
    else:
        return Constraint.Skip
model.sep_incomp_exams = Constraint(model.exams, model.exams, model.time, rule = sep_incomp_exams)



solver = SolverFactory("cbc")
instance = model.create_instance("exam_scheduling.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.exams:
    for j in instance.time:
        if value(instance.sched[i,j]) > 0:
            print(f'Exam {i} is scheduled on time {j}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.exams:
        for j in instance.time:
            if value(instance.sched[i,j]) > 0:
                output.write(f'Exam {i} is scheduled on time {j}\n\n')
    output.close()
