# Consider a set of products to be produced and distributed to a set of 
# customers over a set of time periods. Every customer has a demand that 
# must be satisfied with backlog. Each product has a setup cost given and 
# a unit of every product is associated a processing time for production. 
# Also, for each customer a cost per shipment and inventory costs are given. 
# We wish to determine in each time period how much to produce at the manufacturer, 
# how much to keep in inventory at the manufacturer and at each customer, 
# and how much to ship from the manufacturer to each customer, so that the total 
# cost including production setup, inventory, and transportation, is minimized.

from pyomo.environ import *

model = AbstractModel("Production-Distribution Problem")

#Sets and parameters

#set of manufacturers
model.manufacturer = Set()

#set of customers
model.customers = Set()

#set of all locations
model.locations = model.manufacturer | model.customers

#set of products
model.products = Set()

#set of time periods
model.periods = Set()

#demand of customer k of product j in time period t
model.d = Param(model.products, model.customers, model.periods, within = NonNegativeReals)

#The processing time required for producing one unit of product j
model.p = Param(model.products, within = NonNegativeReals)

#units of production time of a manufacturer during a time period
model.B = Param(within = NonNegativeReals)

#setup cost if product j is produced in a period
model.s = Param(model.products, within = NonNegativeReals)

#unit inventory holding cost of product j in location k
model.h = Param(model.products, model.locations, within = NonNegativeReals)

#initial inventory of product j at location k at time period t
model.I = Param(model.products, model.locations, within = NonNegativeReals)

#Cost of each shipment from the manufacturer to customer k
model.c = Param(model.customers, within = NonNegativeReals)

#the capacity of each shipment
model.capacity = Param(within = NonNegativeReals)

#big M value
model.M = Param(within = NonNegativeReals)

#Variables

#amount of product j produced in period t
model.x = Var(model.products, model.periods, within = NonNegativeReals)

#binary variable indicating whether the production facility is set up for product j in period t
model.y = Var(model.products, model.periods, within = Binary)

#amount of product j delivered to customer k in period t
model.q = Var(model.products, model.customers, model.periods, within = NonNegativeReals)

#Inventory of product j at location k in period t
model.Inv = Var(model.products, model.locations, model.periods, within = NonNegativeReals)

#number of shipments to customer k in period t
model.r = Var(model.customers, model.periods, within = NonNegativeIntegers)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(sum(model.s[j]*model.y[j,t] for j in model.products) for t in model.periods) + sum(sum(sum(model.h[j,k]*model.Inv[j,k,t] for j in model.products) for t in model.periods) for k in model.locations) + sum(sum(model.c[k]*model.r[k,t] for k in model.customers) for t in model.periods)
model.min_cost = Objective(rule = min_cost)

#Constraints

#constraints representing production(capacity and setup)
def cap(model, t):
    return sum(model.p[j]*model.x[j,t] for j in model.products) <= model.B
model.cap = Constraint(model.periods, rule = cap)

def setup(model, j, t):
    return model.x[j,t] <= model.M*model.y[j,t]
model.setup = Constraint(model.products, model.periods, rule = setup)

#Constraint representing the relation of production, inventory, and shipment at the manufacturer
def prod_inv_ship(model, j, t):
    if t == 1:
        return model.Inv[j,0,1] == model.I[j,0] + model.x[j,1] - sum(model.q[j,k,1] for k in model.customers)
    else:
        return model.Inv[j,0,t] == model.Inv[j,0,t-1] + model.x[j,t] - sum(model.q[j,k,t] for k in model.customers)
model.prod_inv_ship = Constraint(model.products, model.periods, rule = prod_inv_ship)

#Constraint representing the relation of shipment, inventory, and demand at each customer
def ship_inv_dem(model, j, k, t):
    if t == 1:
        return model.Inv[j,k,1] == model.I[j,k] + model.q[j,k,1] - model.d[j,k,1]
    else:
        return model.Inv[j,k,t] == model.Inv[j,k,t-1] + model.q[j,k,t] - model.d[j,k,t]
model.ship_inv_dem = Constraint(model.products, model.customers, model.periods, rule = ship_inv_dem)

#Constraint representing the relation between the number of shipments and the amount shipped
def no_shipments_amount_shipped(model, k, t):
    return model.capacity*model.r[k,t] >= sum(model.q[j,k,t] for j in model.products)
model.no_shipments_amount_shipped = Constraint(model.customers, model.periods, rule = no_shipments_amount_shipped)


#Python script for printing the solution in the terminal
solver = SolverFactory("cbc")
instance = model.create_instance("production_distribution.dat")
results = solver.solve(instance)

for i in instance.products:
    for j in instance.periods:
        if value(instance.x[i,j]) > 0:
            print("Product %s will be produced on period %s in amount %f" %(i,j,value(instance.x[i,j])))
for i in instance.products:
    for j in instance.periods:
        if value(instance.y[i,j]) > 0:
            print("Production facility is set up for product %s on period %s" %(i,j))
for i in instance.products:
    for k in instance.customers:
        for j in instance.periods:
            if value(instance.q[i,k,j]) > 0:
                print("Product %s will be delivered to customer %s on period %s in amount %f" %(i,k,j,value(instance.q[i,k,j])))
for i in instance.products:
    for k in instance.locations:
        for j in instance.periods:
            if value(instance.Inv[i,k,j]) > 0:
                print("Inventory of product %s at location %s on period %s in amount %f" %(i,k,j,value(instance.Inv[i,k,j])))
for i in instance.customers:
    for j in instance.periods:
        if value(instance.r[i,j]) > 0:
            print("Number of shipments to customer %s on period %s is: %d" %(i,j,value(instance.r[i,j])))
print("The minimum cost is: %f" %value(instance.min_cost))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.products:
        for j in instance.periods:
            if value(instance.x[i,j]) > 0:
                output.write("Product %s will be produced on period %s in amount %f\n\n" %(i,j,value(instance.x[i,j])))
    for i in instance.products:
        for j in instance.periods:
            if value(instance.y[i,j]) > 0:
                output.write("Production facility is set up for product %s on period %s\n\n" %(i,j))
    for i in instance.products:
        for k in instance.customers:
            for j in instance.periods:
                if value(instance.q[i,k,j]) > 0:
                    output.write("Product %s will be delivered to customer %s on period %s in amount %f\n\n" %(i,k,j,value(instance.q[i,k,j])))
    for i in instance.products:
        for k in instance.locations:
            for j in instance.periods:
                if value(instance.Inv[i,k,j]) > 0:
                    output.write("Inventory of product %s at location %s on period %s in amount %f\n\n" %(i,k,j,value(instance.Inv[i,k,j])))
    for i in instance.customers:
        for j in instance.periods:
            if value(instance.r[i,j]) > 0:
                output.write("Number of shipments to customer %s on period %s is: %d\n\n" %(i,j,value(instance.r[i,j])))
    output.write("The minimum cost is: %f" %value(instance.min_cost))
    output.close()
