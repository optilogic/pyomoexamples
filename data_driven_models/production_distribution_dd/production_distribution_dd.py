# Consider a set of products to be produced and distributed to a set of 
# customers over a set of time periods. Every customer has a demand that 
# must be satisfied with backlog. Each product has a setup cost given and 
# a unit of every product is associated a processing time for production. 
# Also, for each customer a cost per shipment and inventory costs are given. 
# We wish to determine in each time period how much to produce at the manufacturer, 
# how much to keep in inventory at the manufacturer and at each customer, 
# and how much to ship from the manufacturer to each customer, so that the total 
# cost including production setup, inventory, and transportation, is minimized.

from pyomo.environ import *
import os

Files = []
StaticFilenames = ['customers.txt', 'manufactures.txt', 'products.txt', 'time_periods.txt', 'demand.txt', 'processing_time.txt', 'units_of_production.txt', 'setup_cost.txt', 'inventory_cost.txt', 'initial_inventory.txt', 'shipment_cost.txt', 'capacity.txt', 'bigM.txt']

FileHeaders = {}
StaticFileHeaders = {'customers.txt': ['customers'], 'manufactures.txt': ['manufactures'], 'products.txt': ['products'], 'time_periods.txt': ['time_periods'], 'demand.txt': ['customers', 'products', 'time_periods', 'demand'], 'processing_time.txt': ['products', 'processing_time'], 'units_of_production.txt': ['units_of_production'], 'setup_cost.txt': ['products', 'setup_cost'], 'inventory_cost.txt': ['products', 'locations', 'inventory_cost'], 'initial_inventory.txt': ['products', 'locations', 'initial_inventory'], 'shipment_cost.txt': ['customers', 'shipment_cost'], 'capacity.txt': ['capacity'], 'bigM.txt': ['bigM']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation():
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
    	print('The file names inputted do not match the ones listed in StaticFilenames.')
    	quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('production_distribution_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('production_distribution_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['customers.txt', 'products.txt', 'time_periods.txt', 'manufactures.txt']:
        	with open(txtfile, 'r') as readfiles:
            		datafile.write('set ')
            		setnames = txtfile.rsplit('.', 1)[0]
            		datafile.write(setnames)
            		datafile.write(' := \n')
            		next(readfiles)
            		for i in readfiles.read().splitlines():
            			for j in i.split():
            				datafile.write("'")
            				datafile.write(j)
            				datafile.write("'")
            				datafile.write(' ')
            			datafile.write('\n')
            		datafile.write(';')
            		for i in range(2):
            			datafile.write('\n')
        else:
        	datafile.write('param ')
        	with open(txtfile, 'r') as readfiles:
        		paramnames = txtfile.rsplit('.', 1)[0]
        		datafile.write(paramnames)
        		datafile.write(' := \n')
        		next(readfiles)
        		for line in readfiles:
        			line = line.rstrip('\n')
        			for j in range(0, len(line.split('\t'))):
        				if j != len(line.split('\t')) - 1:
        					datafile.write("'")
        					datafile.write(line.split('\t')[j])
        					datafile.write("'")
        					datafile.write(' ')
        				else:
        					datafile.write(line.split('\t')[len(line.split('\t')) - 1])
        					datafile.write('\n')	
        		datafile.write(';')
        		for i in range(2):
        			datafile.write('\n')

model = AbstractModel("Production-Distribution Problem")

#Sets and parameters

#set of manufacturers
model.manufactures = Set()

#set of customers
model.customers = Set()

#set of all locations
model.locations = model.manufactures | model.customers

#set of products
model.products = Set()

#set of time periods
model.time_periods = Set(ordered = True)

#demand of customer k of product j in time period t
model.demand = Param(model.products, model.customers, model.time_periods, within = NonNegativeReals)

#The processing time required for producing one unit of product j
model.processing_time = Param(model.products, within = NonNegativeReals)

#units of production time of a manufacturer during a time period
model.units_of_production = Param(within = NonNegativeReals)

#setup cost if product j is produced in a period
model.setup_cost = Param(model.products, within = NonNegativeReals)

#unit inventory holding cost of product j in location k
model.inventory_cost = Param(model.products, model.locations, within = NonNegativeReals)

#initial inventory of product j at location k at time period t
model.initial_inventory = Param(model.products, model.locations, within = NonNegativeReals)

#Cost of each shipment from the manufacturer to customer k
model.shipment_cost = Param(model.customers, within = NonNegativeReals)

#the capacity of each shipment
model.capacity = Param(within = NonNegativeReals)

#big M value
model.bigM = Param(within = NonNegativeReals)

#Variables

#amount of product j produced in period t
model.x = Var(model.products, model.time_periods, within = NonNegativeReals)

#binary variable indicating whether the production facility is set up for product j in period t
model.y = Var(model.products, model.time_periods, within = Binary)

#amount of product j delivered to customer k in period t
model.q = Var(model.products, model.customers, model.time_periods, within = NonNegativeReals)

#Inventory of product j at location k in period t
model.Inv = Var(model.products, model.locations, model.time_periods, within = NonNegativeReals)

#number of shipments to customer k in period t
model.r = Var(model.customers, model.time_periods, within = NonNegativeIntegers)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(sum(model.setup_cost[j]*model.y[j,t] for j in model.products) for t in model.time_periods) + sum(sum(sum(model.inventory_cost[j,k]*model.Inv[j,k,t] for j in model.products) for t in model.time_periods) for k in model.locations) + sum(sum(model.shipment_cost[k]*model.r[k,t] for k in model.customers) for t in model.time_periods)
model.min_cost = Objective(rule = min_cost)

#Constraints

#constraints representing production(capacity and setup)
def cap(model, t):
    return sum(model.processing_time[j]*model.x[j,t] for j in model.products) <= model.units_of_production
model.cap = Constraint(model.time_periods, rule = cap)

def setup(model, j, t):
    return model.x[j,t] <= model.bigM*model.y[j,t]
model.setup = Constraint(model.products, model.time_periods, rule = setup)

#Constraint representing the relation of production, inventory, and shipment at the manufacturer
def prod_inv_ship(model, j, t):
    if t == model.time_periods[1]:
        return model.Inv[j,'0',model.time_periods[int(t)]] == model.initial_inventory[j,'0'] + model.x[j,model.time_periods[int(t)]] - sum(model.q[j,k,model.time_periods[int(t)]] for k in model.customers)
    else:
        return model.Inv[j,'0',model.time_periods[int(t)]] == model.Inv[j,'0',model.time_periods[int(t)-1]] + model.x[j,model.time_periods[int(t)]] - sum(model.q[j,k,model.time_periods[int(t)]] for k in model.customers)
model.prod_inv_ship = Constraint(model.products, model.time_periods, rule = prod_inv_ship)

#Constraint representing the relation of shipment, inventory, and demand at each customer
def ship_inv_dem(model, j, k, t):
    if t == model.time_periods[1]:
        return model.Inv[j,k,model.time_periods[int(t)]] == model.initial_inventory[j,k] + model.q[j,k,model.time_periods[int(t)]] - model.demand[j,k,model.time_periods[int(t)]]
    else:
        return model.Inv[j,k,model.time_periods[int(t)]] == model.Inv[j,k,model.time_periods[int(t)-1]] + model.q[j,k,model.time_periods[int(t)]] - model.demand[j,k,model.time_periods[int(t)]]
model.ship_inv_dem = Constraint(model.products, model.customers, model.time_periods, rule = ship_inv_dem)

#Constraint representing the relation between the number of shipments and the amount shipped
def no_shipments_amount_shipped(model, k, t):
    return model.capacity*model.r[k,t] >= sum(model.q[j,k,t] for j in model.products)
model.no_shipments_amount_shipped = Constraint(model.customers, model.time_periods, rule = no_shipments_amount_shipped)


#Python script for printing the solution in the terminal
solver = SolverFactory("cbc")
instance = model.create_instance("production_distribution_dd.dat")
results = solver.solve(instance, tee = True)

for i in instance.products:
    for j in instance.time_periods:
        if value(instance.x[i,j]) > 0:
            print("Product %s will be produced on period %s in amount %f" %(i,j,value(instance.x[i,j])))
for i in instance.products:
    for j in instance.time_periods:
        if value(instance.y[i,j]) > 0:
            print("Production facility is set up for product %s on period %s" %(i,j))
for i in instance.products:
    for k in instance.customers:
        for j in instance.time_periods:
            if value(instance.q[i,k,j]) > 0:
                print("Product %s will be delivered to customer %s on period %s in amount %f" %(i,k,j,value(instance.q[i,k,j])))
for i in instance.products:
    for k in instance.locations:
        for j in instance.time_periods:
            if value(instance.Inv[i,k,j]) > 0:
                print("Inventory of product %s at location %s on period %s in amount %f" %(i,k,j,value(instance.Inv[i,k,j])))
for i in instance.customers:
    for j in instance.time_periods:
        if value(instance.r[i,j]) > 0:
            print("Number of shipments to customer %s on period %s is: %d" %(i,j,value(instance.r[i,j])))
print("The minimum cost is: %f" %value(instance.min_cost))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    for i in instance.products:
        for j in instance.time_periods:
            if value(instance.x[i,j]) > 0:
                output.write("Product %s will be produced on period %s in amount %f\n\n" %(i,j,value(instance.x[i,j])))
    for i in instance.products:
        for j in instance.time_periods:
            if value(instance.y[i,j]) > 0:
                output.write("Production facility is set up for product %s on period %s\n\n" %(i,j))
    for i in instance.products:
        for k in instance.customers:
            for j in instance.time_periods:
                if value(instance.q[i,k,j]) > 0:
                    output.write("Product %s will be delivered to customer %s on period %s in amount %f\n\n" %(i,k,j,value(instance.q[i,k,j])))
    for i in instance.products:
        for k in instance.locations:
            for j in instance.time_periods:
                if value(instance.Inv[i,k,j]) > 0:
                    output.write("Inventory of product %s at location %s on period %s in amount %f\n\n" %(i,k,j,value(instance.Inv[i,k,j])))
    for i in instance.customers:
        for j in instance.time_periods:
            if value(instance.r[i,j]) > 0:
                output.write("Number of shipments to customer %s on period %s is: %d\n\n" %(i,j,value(instance.r[i,j])))
    output.write("The minimum cost is: %f" %value(instance.min_cost))
    output.close()
