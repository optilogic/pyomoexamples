# A set of possible warehouse locations along with a set of customers
# are given. Also, there is a cost associated with serving each
# customer to each possible location. It is wished to determine the
# optimal capacitated warehouse locations that will minimize the total
# cost of product delivery.

from pyomo.environ import *
import os

Files = []
StaticFilenames = ['customers.txt', 'facilities.txt', 'facility_cost.txt', 'customer_service_cost.txt']

FileHeaders = {}
StaticFileHeaders = {'customers.txt': ['customers'], 'facilities.txt': ['facilities'], 'facility_cost.txt': ['facilities', 'facility_cost'], 'customer_service_cost.txt': ['customers', 'facilities', 'customer_service_cost']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation():
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
    	print('The file names inputted do not match the ones listed in StaticFilenames.')
    	quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('uncapacitated_facility_location_problem_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('uncapacitated_facility_location_problem_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['customers.txt', 'facilities.txt']:
        	with open(txtfile, 'r') as readfiles:
            		datafile.write('set ')
            		setnames = txtfile.rsplit('.', 1)[0]
            		datafile.write(setnames)
            		datafile.write(' := \n')
            		next(readfiles)
            		for i in readfiles.read().splitlines():
            			for j in i.split():
            				datafile.write("'")
            				datafile.write(j)
            				datafile.write("'")
            				datafile.write(' ')
            			datafile.write('\n')
            		datafile.write(';')
            		for i in range(2):
            			datafile.write('\n')
        else:
        	datafile.write('param ')
        	with open(txtfile, 'r') as readfiles:
        		paramnames = txtfile.rsplit('.', 1)[0]
        		datafile.write(paramnames)
        		datafile.write(' := \n')
        		next(readfiles)
        		for line in readfiles:
        			line = line.rstrip('\n')
        			for j in range(0, len(line.split('\t'))):
        				if j != len(line.split('\t')) - 1:
        					datafile.write("'")
        					datafile.write(line.split('\t')[j])
        					datafile.write("'")
        					datafile.write(' ')
        				else:
        					datafile.write(line.split('\t')[len(line.split('\t')) - 1])
        					datafile.write('\n')	
        		datafile.write(';')
        		for i in range(2):
        			datafile.write('\n')

model=AbstractModel("Uncapacitated Facility Location")

#Sets and Parameters

#set of customers
model.customers = Set()

#set of facilities
model.facilities = Set()

#cost of opening facility j
model.facility_cost = Param(model.facilities, within=NonNegativeReals)

#cost of serving customer i at facility j
model.customer_service_cost = Param(model.customers, model.facilities, within = NonNegativeReals)

#Variables

#binary variable y which is 1 if and only if facility j is open.
model.y = Var(model.facilities, within=Binary)

#binary variable x which is 1 if and only if customer i is assigned/served at facility j
model.x = Var(model.customers, model.facilities, within=Binary)

#Objective

#objective function that minimizes the overall cost
def min_cost(model):
    x=0
    for i in model.customers:
        for j in model.facilities:
            x += model.customer_service_cost[i,j]*model.x[i,j]
    for j in model.facilities:
        x += model.facility_cost[j]*model.y[j]
    return x
model.obj = Objective(rule=min_cost)

# Costraints

#every customer is assigned/served at exactly one facility
def customer_assigned_1_facility(model, i):
    return sum(model.x[i,j] for j in model.facilities) == 1
model.customer_assigned_1_facility = Constraint(model.customers, rule=customer_assigned_1_facility)

#no customer is assigned at a closed facility
def no_customer_closed_facility(model, i, j):
    return model.x[i,j] <= model.y[j]
model.no_customer_closed_facility = Constraint(model.customers, model.facilities, rule=no_customer_closed_facility)


#Python script for printing the solution in the terminal
solver = SolverFactory("cbc")
instance = model.create_instance("uncapacitated_facility_location_problem_dd.dat")
results = solver.solve(instance)

print("The following facilities will open:")
for j in instance.facilities:
    if value(instance.y[j]) > 0:
        print("%s" %j)
for i in instance.customers:
    for j in instance.facilities:
        if value(instance.x[i,j]) > 0:
            print("Customer %s is served at facility %s" %(i,j))
print("The minimum cost is: %f" %value(instance.obj))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    output.write("The following facilities will open:\n")
    for j in instance.facilities:
        if value(instance.y[j]) > 0:
            output.write("%s\n" %j)
    for i in instance.customers:
        for j in instance.facilities:
            if value(instance.x[i,j]) > 0:
                output.write("\nCustomer %s is served at facility %s\n" %(i,j))
    output.write("\nThe minimum cost is: %f" %value(instance.obj))
    output.close()
