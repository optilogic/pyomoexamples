# Assume there are a set of cities that need to be water supplied by a 
# set of reservoirs through pumping stations. Each reservoir has a fixed 
# availability and each pipe in the network has a certain capacity. 
# Each city has a certain demand that need to be satisfied in a decade. 
# We wish to maximize the amount of flow to the sink, while demand constraints are satisfied.

from pyomo.environ import *
import os

Files = []
StaticFilenames = ['nodes.txt', 'sources.txt', 'sinks.txt', 'arcs.txt', 'capacity.txt']

FileHeaders = {}
StaticFileHeaders = {'nodes.txt': ['nodes'], 'sources.txt': ['sources'], 'sinks.txt': ['sinks'], 'arcs.txt': ['nodes', 'nodes'], 'capacity.txt': ['nodes', 'nodes', 'capacity']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation():
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
    	print('The file names inputted do not match the ones listed in StaticFilenames.')
    	quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('water_supply_management_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('water_supply_management_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['nodes.txt', 'sources.txt', 'sinks.txt', 'arcs.txt']:
        	with open(txtfile, 'r') as readfiles:
            		datafile.write('set ')
            		setnames = txtfile.rsplit('.', 1)[0]#.split('.')[0]
            		datafile.write(setnames)
            		datafile.write(' := \n')
            		next(readfiles)
            		for i in readfiles.read().splitlines():
            			for j in i.split():
            				datafile.write("'")
            				datafile.write(j)
            				datafile.write("'")
            				datafile.write(' ')
            			datafile.write('\n')
            		datafile.write(';')
            		for i in range(2):
            			datafile.write('\n')
        else:
        	datafile.write('param ')
        	with open(txtfile, 'r') as readfiles:
        		paramnames = txtfile.rsplit('.', 1)[0]
        		#datafile.write(readfiles.readline().split()[-1])
        		datafile.write(paramnames)
        		datafile.write(' := \n')
        		next(readfiles)
        		for line in readfiles:#.readlines().splitlines():
        			line = line.rstrip('\n')
        			for j in range(0, len(line.split('\t'))):
        				if j != len(line.split('\t')) - 1:
        					datafile.write("'")
        					datafile.write(line.split('\t')[j])
        					datafile.write("'")
        					datafile.write(' ')
        				else:
        					datafile.write(line.split('\t')[len(line.split('\t')) - 1])
        					datafile.write('\n')	
        					#print(line.split('\t')[j])
        		datafile.write(';')
        		for i in range(2):
        			datafile.write('\n')

model = AbstractModel("Water Supply Management/ Maximum flow problem")

#Sets and parameters

#set of all nodes
model.nodes = Set()

#set of arcs between nodes
model.arcs = Set(within=model.nodes*model.nodes)

#set of sources as a subset of nodes
model.sources = Set(within=model.nodes)

#set of sinks as a subset of nodes
model.sinks = Set(within=model.nodes)

#capacities on each arc
model.capacity = Param(model.arcs, within = NonNegativeIntegers)

#Variables

#variable flow indicating the amount of flow from i to j 
model.flow = Var(model.arcs, within=NonNegativeIntegers)

#Objective

#maximizing the flow 
def max_flow(model):
    objective = sum(model.flow[i,j] for (i,j) in model.arcs if j in model.sinks)
    return objective
model.max_flow = Objective(rule = max_flow, sense = maximize)

#Constraints

#conservation constraints
def conservation(model, k):
    if k in model.sources:
        return Constraint.Skip
    elif k in model.sinks:
        return Constraint.Skip
    else:
        return sum(model.flow[i,j] for (i,j) in model.arcs if str(j) == k ) == sum(model.flow[i,j] for (i,j) in model.arcs if str(i) == k)
model.conservation = Constraint(model.nodes, rule = conservation)

#capacity constraints
def cap(model, i,j):
    return model.flow[i,j] <= model.capacity[i,j]
model.cap = Constraint(model.arcs, rule = cap)


solver = SolverFactory("cbc")
instance = model.create_instance("water_supply_management_dd.dat")
results = solver.solve(instance)


#Python Script for printing the solution in the terminal
for (i,j) in instance.arcs:
    if value(instance.flow[i,j]) > 0:
        print("The amount of water from node %s to node %s is: %f" %(i,j,value(instance.flow[i,j])))
print("The maximum amount of flow is: %f" %value(instance.max_flow))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    for (i,j) in instance.arcs:
        if value(instance.flow[i,j]) > 0:
            output.write("The amount of water from node %s to node %s is: %f\n\n" %(i,j,value(instance.flow[i,j])))
    output.write("The maximum amount of flow is: %f" %value(instance.max_flow))
    output.close()
